#!/usr/bin/env bash

#Este script descarga un archivo  con "fecha actual menos los días establecidos en la variable DIAS_DEFAULT 
#desde la fuente vía sftp y lo deja en un directorio hdfs 

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""utils_v2.sh"
source "${bin_path}""../cfg/$1" #Archvio de configuración con los parámetros de conexión

interfaz=$process_name

AVAILABLE_MODES=$modo_cfg
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
        export MODE=$1
else
        echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - " "Archivo de configuración no válido, incluya como primer parámetro el archivo correcto" | tee -a $log_file
        exit 1
fi

if [ "$2" == "" ]; then
  F_D="date --date '-$DIAS_DEFAULT day' +%Y-%m-%d"
  FILE_DATE=$(eval $F_D)
else
  F_D="date -d "$2" +%Y-%m-%d"
  FILE_DATE=$(eval $F_D)
fi
#Log que captura toda la salida del proceso
exec > $log_file 2>&1

log 0 "$log_file" "---------------------------------------------------------- "
log 0 "$log_file" "INICIANDO  ...  $(readlink -f "$0") "
log 0 "$log_file" "El archivo a descargar es: ${prefix_filename}${FILE_DATE}${sufix_filename}"
execute "(sftp -q -oPort=$sftp_port $sftp_username@$sftp_host <<< \"ls ${source_path}${prefix_filename}*${sufix_filename}\")| grep -v ^sftp | rev | cut -d'/' -f 1 | rev | tr -d '[[:blank:]]' | sort > ${tmp_local_path}${server_files}" "Obtencion de archivos en el directorio de ${source_path} exitosa." "Error en la obtencion de archivos en el directorio de: ${source_path}"
#Se guarda el listado de los archivos que hay en el servidor en el momento de la corrida del script
log 0 "$log_file" "---------------------------------------------------------- "
log 0 "$log_file" "LISTADO DE ARCHIVO DISPONIBLES HOY $(date "+%Y-%m-%d-%H:%M:%S") : "
log 0 "$log_file" "  "
cat ${tmp_local_path}${server_files} | tee -a $log_file
log 0 "$log_file" "---------------------------------------------------------- "
if [ $(cat ${tmp_local_path}${server_files} | grep -c "${FILE_DATE}") -ge 1 ]; then
  execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get ${source_path}${prefix_filename}${FILE_DATE}${sufix_filename} ${tmp_download}\"" "Descarga correcta de archivos: ${source_path}${prefix_filename}${FILE_DATE}${sufix_filename}" "Error en la descarga de archivos ${source_path}${prefix_filename}${FILE_DATE}${sufix_filename}"  
  execute "ls ${tmp_download} | grep ${prefix_filename}${FILE_DATE} > ${tmp_local_path}${local_files}" "Se verifica descarga en linux: ${tmp_download} y se registra en: ${tmp_local_path}${local_files}" "Fallo al verificar descarga en linux: ${tmp_download} "
  log 0 "$log_file" "LISTADO DE ARCHIVOS DESCARGADOS HOY $(date "+%Y-%m-%d-%H:%M:%S") : "
  log 0 "$log_file" "  "
  cat ${tmp_local_path}${local_files} | tee -a $log_file
  log 0 "$log_file" "---------------------------------------------------------- "
  if ! [ $(hdfs dfs -ls ${stagepath}${recibidos_folder}${prefix_filename}${FILE_DATE}${sufix_filename} | wc -l) -eq 0 ]; then
  execute "hdfs dfs -rm ${stagepath}${recibidos_folder}${prefix_filename}${FILE_DATE}${sufix_filename}" "Se borran archivos preexistentes en: ${stagepath}${recibidos_folder}" "Error al borrar archivos preexistentes en: ${stagepath}${recibidos_folder}"
  fi
  execute "hdfs dfs -moveFromLocal ${tmp_download}/${prefix_filename}${FILE_DATE}${sufix_filename} ${stagepath}${recibidos_folder}" "Subida con exito de archivos a hdfs :${stagepath}${recibidos_folder}" "Error en la subida de archivos a hdfs: ${stagepath}${recibidos_folder}"
  log 0 "$log_file" "EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE "
  enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
  exit 0
else
  log 2 "$log_file" "No se encontró el archivo ${prefix_filename}${FILE_DATE}${sufix_filename} en la fuente"
  log 2 "$log_file" "EL SCRIPT  $(readlink -f "$0")  FALLÓ "
  enviar_mail "El proceso falló" "$entorno - ERR - $(readlink -f "$0")" 
  exit 1 
fi