#!/bin/bash
# Este proceso se encarga de controlar que los archivos 
# esten bien formados y realizar las llamadas a beeline
# para realizar sus respectivas cargas a la BD.

bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "$bin_path""../cfg/$1" #Archvio de configuración con los parámetros de conexión
source "${bin_path}""utils_v2.sh"    

#Log que captura toda la salida del proceso
exec > $log_file 2>&1

log 0 "$log_file" "'\033[0;32m'INICIANDO  ...  $(readlink -f "$0") '\033[0m' "

function crear_directorio {
    if ! $(hdfs dfs -test -d "$1") ; then
        execute "hdfs dfs -mkdir $1" "Creacion de directorio $1 exitoso" "Error al crear el directio $1."
    else
        log 0 "$log_file" "Directorio $1 ya creado."
    fi
} 

function crear_directorios {
    crear_directorio "$stagepath"  
    crear_directorio "${stagepath}${recibidos_folder}"
    crear_directorio "${stagepath}${procesados_folder}"
    crear_directorio "${stagepath}${invalidos_folder}"
    crear_directorio "${stagepath}${work_folder}"
    crear_directorio "${stagepath}${a_procesar_folder}"
	crear_directorio "${stagepath}${stage_folder}"   
}


crear_directorios

log 0 "$log_file" "'\033[0;32m'EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE  '\033[0m' "
enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
exit 0
