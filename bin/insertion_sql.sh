#!/usr/bin/env bash
# Este proceso se encarga de realizar las llamadas a beeline
# para realizar sus respectivas cargas a la BD.

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""utils_v2.sh"
source "${bin_path}""../cfg/$1" #Archvio de configuración con los parámetros de conexión

AVAILABLE_MODES=$modo_cfg
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
        export MODE=$1
else
        echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - ""Archivo de configuración no válido, los archivos válidos son: "$AVAILABLE_MODES | tee -a $log_file
        exit 1
fi

#Log que captura toda la salida del proceso
exec > $all_log_file 2>&1
log 0 "$log_file" "---------------------------------------------------------- "
log 0 "$log_file" "INICIANDO  ...  $(readlink -f "$0") "

#Si algún archivo quedó en Stage/ se devuelve a A_Procesar/
if [ $(hdfs dfs -ls -C "${stagepath}/${stage_folder}" | wc -l) -gt 0 ]; then 
	(hdfs dfs -ls -C "${stagepath}/${stage_folder}") | while read line;
	do
    file=$(echo $line | rev | cut -d'/' -f 1 | rev)
    if [ $(hdfs dfs -ls "${stagepath}/${a_procesar_folder}${file}" | wc -l) -eq 1 ]; then
		  execute "hdfs dfs -rm ${stagepath}/${a_procesar_folder}${file}" "Archivo preexistente $line removido de ${stagepath}${a_procesar_folder}" "Fallo en la remoción de $line preexistente desde ${stagepath}${a_procesar_folder}"
      execute "hdfs dfs -mv $line ${stagepath}/${a_procesar_folder}" "Move exitoso de $line a ${stagepath}${a_procesar_folder}" "Error en move de $line a ${stagepath}${a_procesar_folder}"
    fi	
  done
fi
#Se verifica que haya archivos para procesar
if [ $(hdfs dfs -ls -C "${stagepath}${a_procesar_folder}" | wc -l) -eq 0 ]; then
	log 2 "${log_file}" "Error, no hay archivos para insertar"
  log 2 "$log_file" "EL SCRIPT  $(readlink -f "$0")  FALLÓ "
  enviar_mail "El proceso falló" "$entorno - ERR - $(readlink -f "$0")" 
  exit 1
else
  execute "hdfs dfs -mv ${stagepath}${a_procesar_folder}* ${stagepath}${stage_folder}" "Se movieron los archivos a: ${stagepath}${stage_folder} " "Error al mover los archivos a: ${stagepath}${stage_folder}"
fi

curr_date="$(date "+%Y%m%d")"
FECHA_PROCESO="'$curr_date'"
HIVE_CONNECTION_STRING="$path_beeline"
HIVEVAR_TEZ_QUEUE="$queue"
HIVEVAR_CURR_DATE="$FECHA_PROCESO"
HIVEVAR_FECHA_A_PROCESAR=""
HIVE_VARS_STRING="$(hive_vars_to_string HIVEVAR_)"
SCRIPT_SQL_FILE="${sql_local_path}${FP_SQL_FILE}"
HIVE_ADDITIONAL_OPTIONS=""
HQL_CMD="execute_hql_script \"$HIVE_CONNECTION_STRING\" \"$HIVE_VARS_STRING\" \"$SCRIPT_SQL_FILE\" \"$HIVE_ADDITIONAL_OPTIONS\""

# Se inserta en la tabla  de fecha proceso _fp
execute "$HQL_CMD" "Se carga $FP_SQL_FILE para la fecha de ingesta $curr_date" "Error en la carga de $FP_SQL_FILE para la fecha de ingesta: ${curr_date}"

# Se buscan las diferentes fechas de eventos para realizar las particiones a partir de estas
execute "beeline -u \"$path_beeline\" --hivevar TEZ_QUEUE=$queue --hivevar CURR_DATE=$FECHA_PROCESO --outputformat=dsv --showHeader=false --delimiterForDSV=\| -f ${sql_local_path}${DISTINCT_DATES_SQL_FILE} > ${tmp_dates}" "Se cargo correctamente el archivo: ${tmp_dates} con las fechas de evento que tiene la particion de $curr_date" "Error al crear archivo: ${tmp_dates} con todas las fechas de evento"
# Iteración por todas las fechas de evento para generar una tabla final para cada una de ellas con las tablas temporales particionadas por fecha de proceso    
log 0 "$log_file" "Iniciando iteración por fechas de evento. Se inserta cada una como particion en la tabla final"
if [ -f "${tmp_dates}" ]
then
  for date in $(cat "${tmp_dates}")
  do
    log 0 "$log_file" "Iniciando carga de ${sql_local_path}${FE_SQL_FILE} PARA LA FECHA: ${date}"
    SCRIPT_SQL_FILE="${sql_local_path}${FE_SQL_FILE}"
    HIVE_VARS_STRING_FECHA=$(echo $HIVE_VARS_STRING | sed -e "s/FECHA_A_PROCESAR=/FECHA_A_PROCESAR='${date}'/g")
    HQL_CMD="execute_hql_script \"$HIVE_CONNECTION_STRING\" \"$HIVE_VARS_STRING_FECHA\" \"$SCRIPT_SQL_FILE\" \"$HIVE_ADDITIONAL_OPTIONS\""
	  execute "$HQL_CMD" "Carga exitosa con beeline de: ${sql_local_path}${FE_SQL_FILE} para la fecha de proceso: $curr_date y fecha de evento $date" "Error en la carga con beeline de ${sql_local_path}${FE_SQL_FILE} para la fecha de proceso $curr_date y fecha de evento $date"
 done
else
  log 2 "$log_file" "Fallo, el archivo: ${tmp_dates} no existe"
  exit 1
fi 
execute "hdfs dfs -rm -skipTrash ${stagepath}${stage_folder}* " "Se eliminaron los archivos de manera exitosa de la carpeta ${stagepath}${stage_folder}" "Error al eliminar los archivos de la carpeta ${stagepath}${stage_folder}"
log 0 "$log_file" "'\033[0;32m'EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE  '\033[0m' "
enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
exit 0
