#!/usr/bin/env bash
# Este script realiza las llamadas a beeline para cargar los archivos 
# en hdfs, primero los carga en una tabla particionada por fecha de proceso y luego los carga en la 
# tabla tabla final particionada por fecha de evento.

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"


bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""utils_v2.sh"
source "${bin_path}""../cfg/$1"

interfaz=$process_name


AVAILABLE_MODES=$modo_cfg
if [[ ",$AVAILABLE_MODES," = *",$1,"* ]]; then
        export MODE=$1
else
        echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - " "Archivo de configuración no válido, incluya como primer parámetro el archivo correcto" | tee -a $log_file
        exit 1
fi

if [[ !($(echo $2 | grep ".sql$"))  ]]; then
  echo -e $(date "+%Y/%m/%d %H:%M:%S")" - [ERR] - ""Error en la ejecución, Parámetro 2 debe ser un archivo .sql válido" | tee -a $log_file
  exit 1
fi 

exec > $log_file 2>&1
log 0 "$log_file" "---------------------------------------------------------- "
log 0 "$log_file" "INICIANDO  ...  $(readlink -f "$0") "

#Si algún archivo quedó en Stage/ se devuelve a A_Procesar/
if [ $(hdfs dfs -ls -C "${stagepath}${stage_folder}" | wc -l) -gt 0 ]; then 
	(hdfs dfs -ls -C "${stagepath}${stage_folder}") | while read line;
	do
    file=$(echo $line | rev | cut -d'/' -f 1 | rev)
    if [ $(hdfs dfs -ls "${stagepath}${a_procesar_folder}${file}" | wc -l) -eq 1 ]; then
		  execute "hdfs dfs -rm ${stagepath}${a_procesar_folder}${file}" "Archivo preexistente $line removido de ${stagepath}${a_procesar_folder}" "Fallo en la remoción de $line preexistente desde ${stagepath}${a_procesar_folder}"
      execute "hdfs dfs -mv $line ${stagepath}${a_procesar_folder}" "Move exitoso de $line a ${stagepath}${a_procesar_folder}" "Error en move de $line a ${stagepath}${a_procesar_folder}"
    else
      execute "hdfs dfs -mv $line ${stagepath}${a_procesar_folder}" "Move exitoso de $line a ${stagepath}${a_procesar_folder}" "Error en move de $line a ${stagepath}${a_procesar_folder}"
    fi	
  done
fi
#Se verifica que haya archivos para procesar
if [ $(hdfs dfs -ls -C "${stagepath}${a_procesar_folder}" | wc -l) -eq 0 ]; then
	log 2 "${log_file}" "Error, no hay nada para insertar en ${process_name}"
  exit 1
else
	(hdfs dfs -ls -C "${stagepath}${a_procesar_folder}") | while read line;
	do
		file=$(echo $line | rev | cut -d'/' -f 1 | rev)
    execute "hdfs dfs -mv $line ${stagepath}${stage_folder}" "Move a Stage exitoso: $line -> ${stagepath}${stage_folder} " "Error en el move de : $line  -> ${stagepath}${stage_folder}"
		log 0 "$log_file" "Iniciando carga de ${stagepath}${stage_folder}${file}"
	done 
fi

	curr_date="$(date "+%Y%m%d")"
	FECHA_PROCESO="'$curr_date'"
    SET_HIVE_CMD="beeline --silent==true"
    SET_HIVE_STR_CON="-u \"$path_beeline\""
    SET_HIVE_VARS="--hivevar TEZ_QUEUE=$queue"
	SET_HIVE_VARS=$SET_HIVE_VARS" --hivevar CURR_DATE="$FECHA_PROCESO
	
    # Se inserta en la tabla ${interfaz}_fp
    execute "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS -f \"${sql_local_path}fp_${interfaz}.sql\"" "Se inserta en la tabla ${interfaz}_fp para la fecha de ingesta $curr_date" "Error en la carga de la tabla ${interfaz}_fp para la fecha de ingesta ${curr_date}"
	
	# Se buscan las diferentes fechas de eventos para realizar las particiones a partir de estas
    execute "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS --outputformat=dsv --showHeader=false --delimiterForDSV=\| -f ${sql_local_path}distinct_dates_${interfaz}.sql > ${sql_local_path}dates/dates_${interfaz}.txt" "Se cargo correctamente el archivo txt con las fechas de evento que tiene la particion de $curr_date, almacenado en: ${sql_dates_path}dates_$interfaz.txt" "Error al crear archivo txt con todas las fechas de evento ${sql_dates_path}dates_$interfaz.txt"
	# cant_lines=$(cat "${sql_local_path}dates/dates_${interfaz}.txt" | wc -l)

	SET_HIVE_VARS=$SET_HIVE_VARS" --hivevar FECHA_A_PROCESAR="
	
	log 0 "$log_file" "Iniciando iteración por fechas de evento. Se inserta cada una como particion en la tabla final ${interfaz}"
	
	# Iteración por todas las fechas de evento para generar una tabla final para cada una de ellas con las tablas temporales particionadas por fecha de proceso    
    for date in $(cat "${sql_local_path}dates/dates_${interfaz}.txt"); do
    log 0 "$log_file" "Iniciando carga de ${sql_local_path}${interfaz}.sql LA FECHA ES ${date}"

		SET_HIVE_VARS_FECHA=$(echo $SET_HIVE_VARS | sed -e "s/FECHA_A_PROCESAR=/FECHA_A_PROCESAR='${date}'/g")
		execute "$SET_HIVE_CMD $SET_HIVE_STR_CON $SET_HIVE_VARS_FECHA -f \"${sql_local_path}${interfaz}.sql\"" "Carga exitosa con beeline de la estructura: ${stagepath}${stage_folder}${file} sobre la fecha de proceso $curr_date y fecha de evento $date" "Error en la carga con beeline de la estructura temporal: ${stagepath}${stage_folder}${file} para la fecha de proceso $curr_date y fecha de evento $date"

    done 
			
	execute "hdfs dfs -rm -skipTrash ${stagepath}${stage_folder}* " "Se eliminaron los archivos de manera exitosa de la carpeta ${stagepath}${stage_folder}" "Error al eliminar los archivos de la carpeta ${stagepath}${stage_folder}"

log 0 "$log_file" "'\033[0;32m'EL SCRIPT  $(readlink -f "$0")  TERMINÓ CORRECTAMENTE  '\033[0m' "
enviar_mail "El proceso terminó correctamente" "$entorno - OK - $(readlink -f "$0")"
exit 0