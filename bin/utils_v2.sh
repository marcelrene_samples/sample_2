#!/usr/bin/env bash
bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/

source "${bin_path}""../cfg/$1"
event_time=$(date "+%Y%m%d%H%M%S")
export log_file="${log_local_path}${2}_$(basename "$0")_${event_time}.log"
#Funciones-----------------------------------------------------

function enviar_mail {
	echo "$1" | mailx -s "$2" -a "$log_file" -a "$err_log_file"  -S smtp=relay.tmoviles.com.ar -S from="hadoop01@tgtarg.com" "marcelr.garcia@telefonica.com" 
    
} 

function execute {
	#Ejecuto el comando y logeo el error si es que hay.
    error=$(eval $1 2>$err_log_file)
    #Detecto si hubo error. 
    if [[ $? -ne 0 ]]; then
    	#Si hay error lo comunico por mail.
        log 2 "$log_file" "$3"
        log 2 "$log_file" "$error"
        enviar_mail "$3" "$entorno - ERROR - $(readlink -f "$0")" 
        exit 1
    else
    	#Logeo la actividad
        log 0 "$log_file" "$2"
    fi
}

#Funcion utilizada para logear actividad de los archivos
function log {
    STATUS=$1
    log_file=$2
    message=$3
    
    prefix=$(date "+%Y/%m/%d %H:%M:%S")
    
    case $STATUS in 
        0) prefix=$prefix" - [INF] - ";;
        1) prefix=$prefix" - [WRN] - ";;
        2) prefix=$prefix" - [ERR] - ";;
        *) prefix=$prefix" - ";;
    esac
    
    echo -e $prefix$message | tee -a $log_file
}

#Calcula las lineas diferentes entre dos archivos
function get_diff {
    execute "comm -23 $1 $2 > $tmp_local_path$file_diff" "Calculado con exito los archivos que tengo que descargar para $3." "Error al calcular los archivos que tengo que descargar para $3."
}

#Calcula las lineas diferentes entre dos archivos uno local con archivos .csv y otro de un servidor conarchivos .bz2
function get_diff_csv_bz2 {
    cat $2 | sed 's/.csv/.csv.bz2/g' > ${tmp_local_path}${local_files}
    sort ${tmp_local_path}${local_files} > 2
    execute "comm -23 $1 $2 > ${tmp_local_path}${file_diff}" "Calculado con exito los archivos que tengo que descargar para $3." "Error al calcular los archivos que tengo que descargar para $3."
}

#Calcula las lineas comunes entre dos archivos  
function get_comm {
    execute "comm -12 $1 $2 > ${tmp_local_path}${common_files}" "Calculado con exito los archivos que tengo que descargar para $3." "Error al calcular los archivos que tengo que descargar para $3."
}

#Calcula las lineas comunes entre dos archivos uno local con archivos .csv y otro de un servidor conarchivos .bz2
function get_comm_csv_bz2 {
    cat $2 | sed 's/.csv/.csv.bz2/g' > ${tmp_local_path}${local_files}
    sort ${tmp_local_path}${local_files} > $2
    execute "comm -12 $1 $2 > ${tmp_local_path}${common_files}" "Calculado con exito los archivos que tengo que descargar para $3." "Error al calcular los archivos que tengo que descargar para $3."
}

#Calcula las lineas comunes entre dos archivos uno local con archivos .bz2 y otro de un servidor conarchivos .csv
function get_comm_bz2_csv {
    cat $2 | sed 's/.csv.bz2/.csv/g' > ${tmp_local_path}${local_files}
    sort ${tmp_local_path}${local_files} > $2
    execute "comm -12 $1 $2 > ${tmp_local_path}${common_files}" "Calculado con exito los archivos que tengo que descargar para $3." "Error al calcular los archivos que tengo que descargar para $3."
}


#Guardo el archivo a la lista de cargados.
# $1 = file
# $2 = Nombre adicional / Estructura
function save_load {
    file="$1"
    file=$(echo "$file" | tr -d '[[:blank:]]')

    #            "ArgentinaIPTV_Devices_2018-06-28_235959.csv >> /cfg/local_list_Devices"
    execute "echo $1 >> $cfg_local_path$local_files$2" "Registrada la carga satisfactoria." "Error al registrar la carga."
}

function load_diff_bz2_standard {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${file_diff} )
    
    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*" 

        #Descargo los archivos faltantes 
        while read  line;
        do
            #Descarga de los archivos que faltan
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
            #Se copia el nombre del archivo sin la extensión .bz2
            file=$(echo ${line} | cut -d'.' -f1-2 )
            #Se verifica que el archivo no esté corrupto
            execute "cat ${tmp_download}${line} | bzip2 -t" "El comprimido NO esta corrupto: ${line}" "El comprimido esta corrupto: ${line}"
            #Se descomprime el archivo para poder numerarlo y continuar con el proceso
            execute "bzip2 -d  ${tmp_download}${line} " "Descompresion exitosa de: ${line}" "Error en la descompresion de: ${line}"
            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${file} | wc -l) -eq 1 ]; then
                    execute "hdfs dfs -rm $3${file}" "Archivo preexistente removido con éxito: $3${file}" "Error al borrar archivo preexistente: $3${file}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs : $3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $file >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $file en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $file en ${cfg_local_path}${local_files}"
            else
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs :$3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados...ESTA INSTRUCCIÓN DEBE IR AL FINALIZAR EL INSERTION DE FORMA EXITOSA......:
            #    execute "echo ${line} >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: ${line} en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: ${line} en ${cfg_local_path}${local_files}"
            fi
        done <  ${tmp_local_path}${file_diff}

    fi
}

function load_diff_standard {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${file_diff} )

    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*"

        #Descargo los archivos faltantes
        while read  line;
        do
            #Descarga de los archivos que faltan
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
            #Se copia el nombre del archivo sin la extensión .bz2
            file=$(echo ${line})
            #Se verifica que el archivo no esté corrupto
            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${file} | wc -l) -eq 1 ]; then
                execute "hdfs dfs -rm $3${file}" "Archivo preexistente removido con éxito: $3${file}" "Error al borrar archivo preexistente: $3${file}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs : $3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $file >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $file en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $file en ${cfg_local_path}${local_files}"
            else
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs :$3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados...ESTA INSTRUCCIÓN DEBE IR AL FINALIZAR EL INSERTION DE FORMA EXITOSA......:
            #    execute "echo ${line} >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: ${line} en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: ${line} en ${cfg_local_path}${local_files}"
            fi
        done <  ${tmp_local_path}${file_diff}

    fi
}

#descarga y numera los archivos anonimizados a partir de la segunda fila (obvia el encabezado)
function load_comm_hash {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${common_files} )

    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*"

        #Descargo los archivos faltantes
        while read  line;
        do
            #Descargo el archivo que me falta
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
            #Se realiza un conteo de los registros del archivo fuente y se registra en el log_file
            row_number_original_file=$(cat ${tmp_download}${line} | wc -l)
	    echo -e "'\033[0;36m'**********************************************************" >> $log_file
            echo -e "El archivo fuente  ${line} tiene ${row_number_original_file} registros " >> $log_file
            echo " " >> $log_file
            echo -e "**********************************************************'\033[0m'" >> $log_file
            #Se numera el archivo anonimizado
	    mv ${tmp_download}${line} ${tmp_download}temp_with_rn
            awk 'NR > 1 {print NR-1 ";" $s}' ${tmp_download}temp_with_rn > ${tmp_download}${line}
            #Se registra el conteo del archivo después de numerarse por el awk en el log_file
            row_number_numbered_file=$(cat ${tmp_download}${line} | wc -l)
            echo -e "'\033[0;36m'**********************************************************" >> $log_file
            echo -e "El archivo modificado con awk  ${line} tiene ${row_number_numbered_file} registros " >> $log_file
            echo " " >> $log_file
            echo -e "**********************************************************'\033[0m'" >> $log_file

            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${line} | wc -l) -eq 1 ]; then
                    execute "hdfs dfs -rm $3${line}" "Archivo preexistente removido con éxito: $3${line}" "Error al borrar archivo preexistente: $3${line}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}$line $3" "Subida con exito al hdfs : $3${line}" "Error en la subida de: $3${line}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $line >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $line en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $line en ${cfg_local_path}${local_files}"
            else
                execute "hdfs dfs -moveFromLocal ${tmp_download}$line $3" "Subida con exito al hdfs :$3${line}" "Error en la subida de: $3${line}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $line >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $line en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $line en ${cfg_local_path}${local_files}"
            fi
        done < ${tmp_local_path}${common_files}
    fi
}
#descarga y numera los archivos con ids para desanonimizar
function load_comm_ids {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${common_files} )

    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*"

        #Descargo los archivos faltantes
        while read  line;
        do
            #Descarga de los archivos que faltan
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
	    #Se copia el nombre del archivo sin la extensión .bz2
            file=$(echo ${line} | cut -d'.' -f1-2 )
	    #Se verifica que el archivo no esté corrupto
	    execute "cat ${tmp_download}${line} | bzip2 -t" "El comprimido NO esta corrupto: ${line}" "El comprimido esta corrupto: ${line}"
            #Se descomprime el archivo para poder numerarlo y continuar con el proceso
            execute "bzip2 -d  ${tmp_download}${line} " "Descompresion exitosa de: ${line}" "Error en la descompresion de: ${line}"
	    #Se renomba y numera el archivo que contiene los ids 
            mv ${tmp_download}${file} ${tmp_download}temp_with_rn
	    awk '{print NR ";" $s}' ${tmp_download}temp_with_rn > ${tmp_download}${file}
            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${file} | wc -l) -eq 1 ]; then
                    execute "hdfs dfs -rm $3${file}" "Archivo preexistente removido con éxito: $3${file}" "Error al borrar archivo preexistente: $3${file}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs : $3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $file >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $file en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $file en ${cfg_local_path}${local_files}"
            else
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs :$3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo ${line} >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: ${line} en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: ${line} en ${cfg_local_path}${local_files}"
            fi
        done <  ${tmp_local_path}${common_files}
    fi
}

function load_comm {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${common_files} )
    
    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*" 

        #Descargo los archivos faltantes 
        while read  line;
        do
            #Descargo el archivo que me falta
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${line} | wc -l) -eq 1 ]; then
	        execute "hdfs dfs -rm $3${line}" "Archivo preexistente removido con éxito: $3${line}" "Error al borrar archivo preexistente: $3${line}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}${line} $3" "Subida con exito al hdfs : $3${line}" "Error en la subida de: $3${line}"
	    #Guardo el el archivo a la lista de cargados.
                execute "echo $line >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $line en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $line en ${cfg_local_path}${local_files}"
	    else
                execute "hdfs dfs -moveFromLocal ${tmp_download}${line} $3" "Subida con exito al hdfs :$3${line}" "Error en la subida de: $3${line}"
            #Guardo el el archivo a la lista de cargados.
            	execute "echo ${line} >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $line en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $line en ${cfg_local_path}${local_files}"
	    fi
        done <  ${tmp_local_path}${common_files} 
    fi
}


function load_diff {
    cant_arch_disponibles=$( wc -l < $tmp_local_path$file_diff )
    
    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        log 0 "$log_file" "*------------------------------------------*"
        log 0 "$log_file" "Cantidad archivos a descargar: $cant_arch_disponibles"
        log 0 "$log_file" "*------------------------------------------*" 

        #Descargo los archivos faltantes 
  


      while read line;
       do
           file_name=$file_init$line$MP_file_ext
            if ! $(hdfs dfs -test -f "$3""/$file_name") ; then
                if ! $(hdfs dfs -test -f "$hdfs_MP_path$collection_folder$procesados_folder""$file_name") ; then
                    #Descargo el archivo que me falta
                    execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2$line/$file_name $tmp_local_path\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
                    execute "hdfs dfs -moveFromLocal $tmp_local_path$file_name $3" "Subida con exito al hdfs del archivo: $line" "Error en la subida de: $line"
                    #Lo agrego a la lista de descargados
                    save_load "$line"
                else
                    log 0 "$log_file" "El archivo $file_name ya esta en la  carpeta de Procesados."
                    save_load "$line"
                fi
            else
                log 0 "$log_file" "El archivo $file_name ya esta en la  carpeta de No Procesados."
                save_load "$line"
            fi
        done < $tmp_local_path$file_diff 
                
        #Ya termino de hacer las cargas
        "$bin_path"decompress.sh
    fi
}

# Descarga los archivos desde el directorio del parametro $2 y los numera a partir de la segunda fila (Se usa para el proceso de desanonimozación de archivos)

function load_diff_ids {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${file_diff} )

    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*"

        #Descargo los archivos faltantes
        while read  line;
        do
            #Descarga de los archivos que faltan
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
            #Se copia el nombre del archivo sin la extensión .bz2
            file=$(echo ${line} | cut -d'.' -f1-2 )
            #Se verifica que el archivo no esté corrupto
            execute "cat ${tmp_download}${line} | bzip2 -t" "El comprimido NO esta corrupto: ${line}" "El comprimido esta corrupto: ${line}"
            #Se descomprime el archivo para poder numerarlo y continuar con el proceso
            execute "bzip2 -d  ${tmp_download}${line} " "Descompresion exitosa de: ${line}" "Error en la descompresion de: ${line}"
            #Se renomba y numera el archivo que contiene los ids
            mv ${tmp_download}${file} ${tmp_download}temp_with_rn
            awk '{print NR ";" $s}' ${tmp_download}temp_with_rn > ${tmp_download}${file}
            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${file} | wc -l) -eq 1 ]; then
                    execute "hdfs dfs -rm $3${file}" "Archivo preexistente removido con éxito: $3${file}" "Error al borrar archivo preexistente: $3${file}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs : $3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $file >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $file en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $file en ${cfg_local_path}${local_files}"
            else
                execute "hdfs dfs -moveFromLocal ${tmp_download}$file $3" "Subida con exito al hdfs :$3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados...ESTA INSTRUCCIÓN DEBE IR AL FINALIZAR EL INSERTION DE FORMA EXITOSA......:
            #    execute "echo ${line} >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: ${line} en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: ${line} en ${cfg_local_path}${local_files}"
            fi
        done <  ${tmp_local_path}${file_diff}
    fi
}


function load_diff_hash {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${file_diff} )
    
    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio de: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*" 

        #Descargo los archivos faltantes 
        while read  line;
        do
            #Descargo el archivo que me falta
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"get $2/$line ${tmp_download}\"" "Descarga correcta del archivo: $line" "Error en la descarga de: $line"
            #Numero el archivo hasheado
            mv ${tmp_download}${line} ${tmp_download}temp_with_rn
            awk 'NR > 1 {print NR-1 ";" $s}' ${tmp_download}temp_with_rn > ${tmp_download}${line} 
            #Lo cargo al HDFS
            if [ $(hdfs dfs -ls $3${line} | wc -l) -eq 1 ]; then
	            execute "hdfs dfs -rm $3${line}" "Archivo preexistente removido con éxito: $3${line}" "Error al borrar archivo preexistente: $3${line}"
                execute "hdfs dfs -moveFromLocal ${tmp_download}$line $3" "Subida con exito al hdfs : $3${line}" "Error en la subida de: $3${line}"
	    #Guardo el el archivo a la lista de cargados.
                execute "echo $line >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $line en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $line en ${cfg_local_path}${local_files}"
	    else
                execute "hdfs dfs -moveFromLocal ${tmp_download}$line $3" "Subida con exito al hdfs :$3${line}" "Error en la subida de: $3${line}"
            #Guardo el el archivo a la lista de cargados...ESTA INSTRUCCIÓN DEBE IR AL FINALIZAR EL INSERTION DE FORMA EXITOSA......:
            #	execute "echo $line >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $line en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $line en ${cfg_local_path}${local_files}"
	    fi
        done <  ${tmp_local_path}${file_diff} 
    fi
}

#Dado un arreglo de tablas, busco en la BD y mando un informe con el COUNT(*) de los ultimos dias
#Parametros: 
#   3 = Arch. temporal.
#   2 = Cant. Dias.
#   1 = Particion
#   $@ = Tabla/s

#Es necesario para los archivos hasheados. Ya que despues de esta fecha se comenzaron a encriptar los archivos.
# Por lo tanto si se corre el getter y esto no, no se van a deshashear en la primera corrida xq el raw_group se genera
# antes de comenzar para una estructura.
function carga_inicial {
        echo "OBArgentina_Users_2018-08-24_235959.csv" > "$tmp_local_path""diff"
        load_diff "Users" "$sftp_ott_path""Users" "$hdfs_OTT_path""Users/$no_procesados_folder"
}

#Limpia el path destino : 
#	$1 = Path destino 
function limpiar_directorio {
	isEmpty=$(hdfs dfs -count "$1" | awk '{print $2}')

	if [[ $isEmpty -ne 0 ]]; 
	then
		execute "hdfs dfs -rm -R \"$1/*\"" "Limpiado con existo el directorio $1." "Error al limpiar el directorio $1."
	else
    	log 0 "$log_file" "No hay archivos para eliminar en $1"
	fi
}

function informar_tablas_particionadas {

estructuras_arr=("$@")
particion="$2"
cant_dias="$3"
temp_file="$4"

#Inicializo archivo donde se van a guardar los resultados. 
echo " " > "$temp_file"

for estructura in "${estructuras_arr[@]}";
do
    #Inicializo la estructura
    echo "Valores obtenidos en $estructura:" >> "$temp_file"
    
    #Obtengo las particiones
    valor=$(beeline --silent==true --showHeader=false --outputformat=tsv2 -u "jdbc:hive2://aphdpmngtp10.tmoviles.com.ar:2181,aphdpmngtp11.tmoviles.com.ar:2181,aphdpmngtp09.tmoviles.com.ar:2181,aphdpmngtp07.tmoviles.com.ar:2181,aphdpmngtp08.tmoviles.com.ar:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2;principal=hive/_HOST@TASA.COM" -e "SHOW PARTITIONS $estructura")
    
    #Obtengo la cantidad de datos ingresados en los ultimos siete dias
    echo "Calculando los valores de $estructura ..":
    (echo "$valor" | tail -"$cant_dias" | cut -d'=' -f2) | while read line;
    do
        cant=$(beeline --silent==true --showHeader=false --outputformat=tsv2 -u "jdbc:hive2://aphdpmngtp10.tmoviles.com.ar:2181,aphdpmngtp11.tmoviles.com.ar:2181,aphdpmngtp09.tmoviles.com.ar:2181,aphdpmngtp07.tmoviles.com.ar:2181,aphdpmngtp08.tmoviles.com.ar:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2;principal=hive/_HOST@TASA.COM" -e "SELECT COUNT (*) FROM $estructura WHERE $particion""=$line")
        echo "$line     $cant" >> "$temp_file"
    done
    
    #Agrego una linea para mejor claridad
    echo " " >> "$temp_file"
done

ult_res_global=$(cat "$temp_file")

enviar_mail "$ult_res_global" "Resultados ultima subida exitosa IPTV"
}

function move_files {
    cant_arch_disponibles=$( wc -l < $1 )

    if [ $cant_arch_disponibles -eq 0 ] ; then
        log 1 "$log_file" "No hay archivos nuevos para mover desde $2 hacia $3 $(date "+%Y-%m-%d-%H:%M:%S") : "
    else
        log 0 "$log_file" "Se van a mover \"$cant_arch_disponibles\" archivos desde: $2 hacia $3 $(date "+%Y-%m-%d-%H:%M:%S") : "

        #Se leen todos los archivos descargados hoy
        while read  line;
        do
            #Se mueven los archivos desde $2 hacia $3
            execute "sftp -oPort=$sftp_port $sftp_username@$sftp_host <<< \"rename ${2}${line} ${3}${line}\"" "Movimiento correcto del archivo: $2$line hacia $3" "Error en el movimiento del archivo: $2$line hacia $3"
            
        done <  $1

    fi
}

function load_diff_local {
    cant_arch_disponibles=$( wc -l < ${tmp_local_path}${file_diff} )

    if [ $cant_arch_disponibles -eq 0 ] ; then
        echo "*------------------------------------------*"
        echo "Todavia no hay archivos nuevos para subir."
        echo "Estructura: $1"
        enviar_mail "No se encontraron archivos para subir en el directorio: $1" "No se encontraron archivos $1.."
        echo "*------------------------------------------*"
    else
        echo "*------------------------------------------*"
        echo "Cantidad archivos a descargar: $cant_arch_disponibles"
        echo "*------------------------------------------*"
        #Descargo los archivos faltantes
        while read  line;
        do
            file=$(echo $line | rev | cut -d'/' -f 1 | rev)
            #Carga del archivo del directorio local de linux al HDFS
            if [ $(hdfs dfs -ls $3${file} | wc -l) -eq 1 ]; then
                execute "hdfs dfs -rm $3${file}" "Archivo preexistente removido con éxito: $3${file}" "Error al borrar archivo preexistente: $3${file}"
                execute "hdfs dfs -mv $2${file} $3" "Subida con exito al hdfs : $3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados.
                execute "echo $file >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: $file en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: $file en ${cfg_local_path}${local_files}"
            else
                execute "hdfs dfs -mv $2${file} $3" "Subida con exito al hdfs :$3${file}" "Error en la subida de: $3${file}"
            #Guardo el el archivo a la lista de cargados...ESTA INSTRUCCIÓN DEBE IR AL FINALIZAR EL INSERTION DE FORMA EXITOSA......:
                execute "echo ${line} >> ${cfg_local_path}${local_files}" "Registro con éxito del archivo: ${line} en ${cfg_local_path}${local_files}" "Fallo en el registro del archivo: ${line} en ${cfg_local_path}${local_files}"
            fi
        done <  ${tmp_local_path}${file_diff}

    fi
}

function execute_hql_script(){
    # Ejecuta un script HQL
    # Parametros:
    #  - $1: Connection String
    #  - $2: String con los hivevars
    #  - $3: Archivo HQL a ejecutar (incluyendo path)
    #  - $4: String con opciones adicionales

    local __HIVE_CONNECTION_STRING="$1"
    local __HIVE_VARS_STRING="$2"
    local __HQL_FILE="$3"
    local __HIVE_ADDITIONAL_OPTIONS="$4"

    echo "Ejecutando: beeline -u $__HIVE_CONNECTION_STRING 
                                 $__HIVE_VARS_STRING 
		                 -f $__HQL_FILE 
                                 $__HIVE_ADDITIONAL_OPTIONS"

	# Ejecuto la query para cargar el crudo.
    beeline -u $__HIVE_CONNECTION_STRING \
                              $__HIVE_VARS_STRING \
                              -f $__HQL_FILE \
                              $__HIVE_ADDITIONAL_OPTIONS 2>&1

    if [ $? -ne 0 ] ; then
      echo "EJECUTADO CON ERROR"
      return 1
    else
      echo "EJECTADO OK"
      return 0
    fi
}

function hive_vars_to_string(){
  # Genera un string con "--hivevar VAR1=VALUE_VAR1 --hivevar VAR2=VALUE_VAR2 ... --hivevar VARN=VALUE_VARN"
    # Donde VAR# es una variable que comienzan el prefijo PRE_ pasado como parametro
    # es decir existen PRE_VAR1 a PRE_VARN y VALUE_VAR1 es el valor de PRE_VAR1 (y asi)
    # Parametros:
    #   $1: es el prefijo para el cual se toma las variables
    # Ejemplo:
    #   HIVEVAR_UNO="UNO_VALUE"
    #   HIVEVAR_DOS="DOS_VALUE"
    #   HIVEVAR_TRES="TRES_VALUE"
    #  hive_vars_to_string "HIVEVAR_" retornara:
    #   ' --hivevar DOS=DOS_VALUE --hivevar TRES=TRES_VALUE --hivevar UNO=UNO_VALUE'

  PREFFIX=$1
  HIVEVARS_STRING=""
  for HIVEVAR in $(compgen -v "$PREFFIX")
  do
    HIVEVARS_STRING="${HIVEVARS_STRING} --hivevar ${HIVEVAR#$PREFFIX}=${!HIVEVAR}"    
  done
  echo "$HIVEVARS_STRING"
  return 0
}
