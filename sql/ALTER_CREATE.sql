-- PURCHASE 
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Purchases ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Purchases ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Purchases ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Purchases ADD COLUMNS(fecha_ingesta timestamp);

-----------------------------------------------------------------------------------------
-- DEVICE

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_devices ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_devices ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_devices ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_devices ADD COLUMNS(fecha_ingesta timestamp);

-----------------------------------------------------------------------------------------
-- USER 

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_users ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_users ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_users ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_users ADD COLUMNS(fecha_ingesta timestamp);

-----------------------------------------------------------------------------------------

-- UsersPreferences

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_UsersPreferences ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_UsersPreferences ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_UsersPreferences ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_UsersPreferences ADD COLUMNS(fecha_ingesta timestamp);

------------------------------------------------------------------------------------------

-- GVP_IPTV_SubscriptionsLiveChannels


ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_SubscriptionsLiveChannels ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_SubscriptionsLiveChannels ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_SubscriptionsLiveChannels ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_SubscriptionsLiveChannels ADD COLUMNS(fecha_ingesta timestamp);


-------------------------------------------------------------------------------------------

-- GVP_IPTV_Subscriptions

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Subscriptions ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Subscriptions ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Subscriptions ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Subscriptions ADD COLUMNS(fecha_ingesta timestamp);

---------------------------------------------------------------------------------------------

-- GVP_IPTV_Products

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Products ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Products ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Products ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Products ADD COLUMNS(fecha_ingesta timestamp);

-----------------------------------------------------------------------------------------------

-- GVP_IPTV_Playbacks

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Playbacks ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Playbacks ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Playbacks ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Playbacks ADD COLUMNS(fecha_ingesta timestamp);

------------------------------------------------------------------------------------------------

-- GVP_IPTV_Movies

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Movies ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Movies ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Movies ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Movies ADD COLUMNS(fecha_ingesta timestamp);

--------------------------------------------------------------------------------------------------


-- GVP_IPTV_Mmedias

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Mmedias ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Mmedias ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Mmedias ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Mmedias ADD COLUMNS(fecha_ingesta timestamp);

----------------------------------------------------------------------------------------------------


-- GVP_IPTV_Login

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Login ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_Login ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Login ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Login ADD COLUMNS(fecha_ingesta timestamp);

----------------------------------------------------------------------------------------------------

-- GVP_IPTV_LiveChannels

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_LiveChannels ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_LiveChannels ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_LiveChannels ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_LiveChannels ADD COLUMNS(fecha_ingesta timestamp);

---------------------------------------------------------------------------------------------------------


-- GVP_IPTV_ChannelMaps

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_ChannelMaps ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_ChannelMaps ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_ChannelMaps ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_ChannelMaps ADD COLUMNS(fecha_ingesta timestamp);

----------------------------------------------------------------------------------------------------------

-- GVP_IPTV_UsersTags

ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_UsersTags ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_IPTV_UsersTags ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_UsersTags ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_UsersTags ADD COLUMNS(fecha_ingesta timestamp);

-------------------------------------------------------------------------------------------------------------

-- GVP_OTT_Youbora

ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Youbora ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_OTT_Youbora ADD COLUMNS(fecha_ingesta timestamp);

ALTER TABLE ${DBNAME_TRAFICO}.GVP_KPI_Youbora ADD COLUMNS(fecha_archivo string);
ALTER TABLE ${DBNAME_TRAFICO}.GVP_KPI_Youbora ADD COLUMNS(fecha_ingesta timestamp);

------------------------------------------------------------------------------------------------------------

-------------------------------------------- Money Map -------------------------------------
DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_devices_mm_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_devices_mm_vw AS
SELECT
  creationdate,
  devicetype,
  mibid,
  name,
  deviceid,
  externalid_mac,
  enabled,
  userid,
  useruniqueid,
  useremail,
  notificationtoken,
  tokenenabled,
  master,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_iptv_devices
WHERE
  day=from_unixtime(unix_timestamp(date_sub(current_date(),1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_devices_mm_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_devices_mm_vw AS
SELECT
  creationdate,
  devicetype,
  mibid,
  name,
  deviceid,
  externalid_mac,
  enabled,
  userid,
  useruniqueid,
  useremail,
  notificationtoken,
  tokenenabled,
  master,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_ott_devices
WHERE
  day=from_unixtime(unix_timestamp(date_sub(current_date(),1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_purchases_mm_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_purchases_mm_vw AS
SELECT
  datetime_dt,
  producttype,
  productid,
  product,
  productname,
  producername,
  distributorname,
  commercializationtype,
  contentcategoryid,
  tvodtypeid,
  quality,
  price,
  useremail,
  useruniqueid,
  firstname,
  lastname,
  servicetype,
  devicetypename,
  paymenttype,
  webpaytransactionid,
  idwebpay,
  authwebpaycode,
  purchasestatus,
  transactionhash,
  syncmessage,
  deviceid,
  billingid,
  origin_file_name,
  datetime_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(datetime_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_iptv_purchases
WHERE
  day>=from_unixtime(unix_timestamp(date_sub(current_date(),1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_purchases_mm_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_purchases_mm_vw AS
SELECT
  datetime_dt,
  producttype,
  productid,
  product,
  productname,
  producername,
  distributorname,
  commercializationtype,
  contentcategoryid,
  tvodtypeid,
  quality,
  price,
  useremail,
  useruniqueid,
  firstname,
  lastname,
  servicetype,
  devicetypename,
  paymenttype,
  webpaytransactionid,
  idwebpay,
  authwebpaycode,
  purchasestatus,
  transactionhash,
  syncmessage,
  deviceid,
  billingid,
  origin_file_name,
  datetime_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(datetime_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_ott_purchases
WHERE
  day>=from_unixtime(unix_timestamp(date_sub(current_date(),1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_users_mm_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_users_mm_vw AS
SELECT  
  creationdate,
  mibid,
  useruniqueid,
  useremail,
  servicetype,
  status,
  pvr,
  geographicarea,
  blackoutarea,
  timezone,
  sdmediastreams,
  hdmediastreams,
  dvbttuners,
  currentcreditlimit,
  typeid,
  creditcardstatus,
  subscriptions,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM 
  ${DBNAME_TRAFICO}.gvp_iptv_users
WHERE
  day=from_unixtime(unix_timestamp(date_sub(current_date(),1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_users_mm_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_users_mm_vw AS
SELECT 
  creationdate,
  mibid,
  useruniqueid,
  useremail,
  servicetype,
  status,
  pvr,
  geographicarea,
  blackoutarea,
  timezone,
  sdmediastreams,
  hdmediastreams,
  dvbttuners,
  currentcreditlimit,
  typeid,
  creditcardstatus,
  subscriptions,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_ott_users
WHERE
  day=from_unixtime(unix_timestamp(date_sub(current_date(),1),'yyyy-mm-dd'),'yyyymmdd');

	
-------------------------------------------- USUARIO -------------------------------------

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_devices_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_devices_vw AS
SELECT
  creationdate,
  devicetype,
  mibid,
  name,
  deviceid,
  externalid_mac,
  enabled,
  userid,
  useruniqueid,
  useremail,
  notificationtoken,
  tokenenabled,
  master,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_iptv_devices
WHERE
  day>=from_unixtime(unix_timestamp(add_months(current_date(),-1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_devices_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_devices_vw AS
SELECT
  creationdate,
  devicetype,
  mibid,
  name,
  deviceid,
  externalid_mac,
  enabled,
  userid,
  useruniqueid,
  useremail,
  notificationtoken,
  tokenenabled,
  master,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_ott_devices
WHERE
  day>=from_unixtime(unix_timestamp(add_months(current_date(),-1),'yyyy-mm-dd'),'yyyymmdd');


DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_purchases_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_purchases_vw AS
SELECT
  datetime_dt,
  producttype,
  productid,
  product,
  productname,
  producername,
  distributorname,
  commercializationtype,
  contentcategoryid,
  tvodtypeid,
  quality,
  price,
  useremail,
  useruniqueid,
  firstname,
  lastname,
  servicetype,
  devicetypename,
  paymenttype,
  webpaytransactionid,
  idwebpay,
  authwebpaycode,
  purchasestatus,
  transactionhash,
  syncmessage,
  deviceid,
  billingid,
  origin_file_name,
  datetime_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(datetime_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_iptv_purchases
WHERE
  day>=from_unixtime(unix_timestamp(add_months(current_date(),-1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_purchases_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_purchases_vw AS
SELECT
  datetime_dt,
  producttype,
  productid,
  product,
  productname,
  producername,
  distributorname,
  commercializationtype,
  contentcategoryid,
  tvodtypeid,
  quality,
  price,
  useremail,
  useruniqueid,
  firstname,
  lastname,
  servicetype,
  devicetypename,
  paymenttype,
  webpaytransactionid,
  idwebpay,
  authwebpaycode,
  purchasestatus,
  transactionhash,
  syncmessage,
  deviceid,
  billingid,
  origin_file_name,
  datetime_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(datetime_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_ott_purchases
WHERE
  day>=from_unixtime(unix_timestamp(add_months(current_date(),-1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_users_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_iptv_users_vw AS
SELECT  
  creationdate,
  mibid,
  useruniqueid,
  useremail,
  servicetype,
  status,
  pvr,
  geographicarea,
  blackoutarea,
  timezone,
  sdmediastreams,
  hdmediastreams,
  dvbttuners,
  currentcreditlimit,
  typeid,
  creditcardstatus,
  subscriptions,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM 
  ${DBNAME_TRAFICO}.gvp_iptv_users
WHERE
  day>=from_unixtime(unix_timestamp(add_months(current_date(),-1),'yyyy-mm-dd'),'yyyymmdd');

DROP VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_users_vw;
CREATE VIEW ${DBNAME_ASEG_INGRESOS}.gvp_ott_users_vw AS
SELECT 
  creationdate,
  mibid,
  useruniqueid,
  useremail,
  servicetype,
  status,
  pvr,
  geographicarea,
  blackoutarea,
  timezone,
  sdmediastreams,
  hdmediastreams,
  dvbttuners,
  currentcreditlimit,
  typeid,
  creditcardstatus,
  subscriptions,
  origin_file_name,
  creationdate_fh_ar,
  fecha_archivo,
  from_unixtime(unix_timestamp(fecha_ingesta,'yyyy-MM-dd hh:mm:ss'),'yyyyMMddhhmmss') fecha_ingesta,
  day,
  from_unixtime(unix_timestamp(creationdate_fh_ar,'yyyy-MM-dd HH:mm:ss'),'yyyyMMdd') fecha_alta
FROM
  ${DBNAME_TRAFICO}.gvp_ott_users
WHERE
  day>=from_unixtime(unix_timestamp(add_months(current_date(),-1),'yyyy-mm-dd'),'yyyymmdd');

