-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_IPTV_ChannelMaps;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_IPTV_ChannelMaps(
CREATIONDATE                STRING,
ID                          STRING,
NAME_DE                     STRING,
TITLE                       STRING,
LIVECHANNELID               STRING,
QUALITY                     STRING,
REGIONID                    STRING,
REGIONNAME                  STRING,
REGIONGEOGRAPHICAREACODE    STRING,
TECHNOLOGIESID              STRING,
TECHNOLOGIESNAME            STRING,
TECHNOLOGIESTITLE           STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/IPTV_ChannelMaps/Stage/" 
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/IPTV/ChannelMaps/
DROP TABLE IF EXISTS TRAFICO.GVP_IPTV_ChannelMaps;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_IPTV_ChannelMaps(
CREATIONDATE                INT,
ID                          INT,
NAME_DE                     STRING,
TITLE                       STRING,
LIVECHANNELID               INT,
QUALITY                     INT,
REGIONID                    INT,
REGIONNAME                  STRING,
REGIONGEOGRAPHICAREACODE    INT,
TECHNOLOGIESID              INT,
TECHNOLOGIESNAME            STRING,
TECHNOLOGIESTITLE           STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;