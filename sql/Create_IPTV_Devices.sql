-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_IPTV_Devices;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_IPTV_Devices(
CREATIONDATE            String,
DEVICETYPE              String,
MIBID                   String,
NAME                    String,
DEVICEID                String,
EXTERNALID_MAC          String,
ENABLED                 String,
USERID                  String,
USERUNIQUEID            String,
USEREMAIL               String,
NOTIFICATIONTOKEN       String,
TOKENENABLED            String,
MASTER                  String,
ORIGIN_FILE_NAME        String,
CREATIONDATE_FH_AR      STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/IPTV_Devices/Stage/"
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/IPTV/Devices*/
DROP TABLE IF EXISTS TRAFICO.GVP_IPTV_Devices;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_IPTV_Devices(
CREATIONDATE            Int,
DEVICETYPE              String,
MIBID                   Int,
NAME                    String,
DEVICEID                String,
EXTERNALID_MAC          String,
ENABLED                 Int,
USERID                  Int,
USERUNIQUEID            String,
USEREMAIL               String,
NOTIFICATIONTOKEN       String,
TOKENENABLED            Boolean,
MASTER                  Boolean,
ORIGIN_FILE_NAME        String,
CREATIONDATE_FH_AR      timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;