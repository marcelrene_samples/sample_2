-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_IPTV_Login;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_IPTV_Login(
DATETIME_DT             String,
DEVICETYPEUSED          String,
DEVICEID                String,
STATUSMESSAGE           String,
STATUSCODE              String,
USERTYPE                String, 
UNIQUEUSERCODE          String,
METHOD                  String,
SESSIONID               String,
USERID                  String,
ORIGIN_FILE_NAME        String,
DATETIME_FH_AR          String
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/IPTV_Login/Stage/"
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/IPTV/Login*/
DROP TABLE IF EXISTS TRAFICO.GVP_IPTV_Login;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_IPTV_Login(
DATETIME_DT             Int,
DEVICETYPEUSED          Int,
DEVICEID                Int,
STATUSMESSAGE           String,
STATUSCODE              Int,
USERTYPE                Int, 
UNIQUEUSERCODE          String,
METHOD                  Int,
SESSIONID               String,
USERID                  String,
ORIGIN_FILE_NAME        String,
DATETIME_FH_AR          TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;