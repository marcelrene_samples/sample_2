-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_IPTV_Mmedias;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_IPTV_Mmedias(
MOVIEID                     STRING,
ID                          STRING,
QUALITY                     STRING,
TYPE_DE                     STRING,
LANGUAGE_DE                 STRING,
LANGUAGELIST                STRING,
CDNSTATUS                   STRING,
PLAYTYPE                    STRING,
ORIGIN_FILE_NAME            STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/IPTV_Mmedias/Stage/" 
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/IPTV/Mmedias/
DROP TABLE IF EXISTS TRAFICO.GVP_ITPV_Mmedias;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_IPTV_Mmedias(
MOVIEID                     INT,
ID                          INT,
QUALITY                     STRING,
TYPE_DE                     STRING,
LANGUAGE_DE                 STRING,
LANGUAGELIST                STRING,
CDNSTATUS                   STRING,
PLAYTYPE                    STRING,
ORIGIN_FILE_NAME            STRING
) PARTITIONED BY (day INT)
STORED AS ORC;