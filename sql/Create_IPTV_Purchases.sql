-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_IPTV_Purchases;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_IPTV_Purchases(
DATETIME_DT	            String,
PRODUCTTYPE	            String,
PRODUCTID	            String,
PRODUCT	                String,
PRODUCTNAME	            String,
PRODUCERNAME	        String,
DISTRIBUTORNAME	        String,
COMMERCIALIZATIONTYPE	String,
CONTENTCATEGORYID	    String,
TVODTYPEID	            String,
QUALITY	                String,
PRICE	                String,
USEREMAIL	            String,
USERUNIQUEID	        String,
FIRSTNAME	            String,
LASTNAME	            String,
SERVICETYPE	            String,
DEVICETYPENAME	        String,
PAYMENTTYPE	            String,
WEBPAYTRANSACTIONID	    String,
IDWEBPAY	            String,
AUTHWEBPAYCODE	        String,
PURCHASESTATUS	        String,
TRANSACTIONHASH	        String,
SYNCMESSAGE	            String,
DEVICEID	            String,
BILLINGID	            String,
ORIGIN_FILE_NAME        String,
DATETIME_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/IPTV_Purchases/Stage/"
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/IPTV/Purchases */
DROP TABLE IF EXISTS TRAFICO.GVP_IPTV_Purchases;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_IPTV_Purchases(
DATETIME_DT	            Int,
PRODUCTTYPE	            String,
PRODUCTID	            Int,
PRODUCT	                Int,
PRODUCTNAME	            String,
PRODUCERNAME	        String,
DISTRIBUTORNAME	        String,
COMMERCIALIZATIONTYPE	Int,
CONTENTCATEGORYID	    Int,
TVODTYPEID	            Int,
QUALITY	                String,
PRICE	                Int,
USEREMAIL	            String,
USERUNIQUEID	        String,
FIRSTNAME	            String,
LASTNAME	            String,
SERVICETYPE	            String,
DEVICETYPENAME	        String,
PAYMENTTYPE	            String,
WEBPAYTRANSACTIONID	    String,
IDWEBPAY	            String,
AUTHWEBPAYCODE	        String,
PURCHASESTATUS	        Int,
TRANSACTIONHASH	        String,
SYNCMESSAGE	            String,
DEVICEID	            String,
BILLINGID	            String,
ORIGIN_FILE_NAME        String,
DATETIME_FH_AR          TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;