-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_IPTV_Subscriptions;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_IPTV_Subscriptions(
CREATIONDATE                STRING,
USERID                      STRING,
USERUNIQUEID                STRING,
USEREMAIL                   STRING,
STATUS_DE                   STRING,
SUBSCRIPTIONID              STRING,
SUBSCRIPTIONNAME            STRING,
PAYMENTTYPE                 STRING,
DATESTART                   STRING,
DATEEND                     STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING,
DATESTART_FH_AR             STRING,
DATEEND_FH_AR               STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/IPTV_Subscriptions/Stage/" --VERIFICAR LOCATION
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/IPTV/Subscriptions/
DROP TABLE IF EXISTS TRAFICO.GVP_IPTV_Subscriptions;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_IPTV_Subscriptions(
CREATIONDATE                INT,
USERID                      STRING,
USERUNIQUEID                STRING,
USEREMAIL                   STRING,
STATUS_DE                   STRING,
SUBSCRIPTIONID              INT,
SUBSCRIPTIONNAME            STRING,
PAYMENTTYPE                 INT,
DATESTART                   INT,
DATEEND                     INT,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          TIMESTAMP,
DATESTART_FH_AR             TIMESTAMP,
DATEEND_FH_AR               TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;