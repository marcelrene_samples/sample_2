-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_ChannelMaps;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_ChannelMaps(
CREATIONDATE                STRING,
ID                          STRING,
NAME_DE                     STRING,
TITLE                       STRING,
LIVECHANNELID               STRING,
QUALITY                     STRING,
REGIONID                    STRING,
REGIONNAME                  STRING,
REGIONGEOGRAPHICAREACODE    STRING,
TECHNOLOGIESID              STRING,
TECHNOLOGIESNAME            STRING,
TECHNOLOGIESTITLE           STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_ChannelMaps/Stage/" 
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/ChannelMaps/
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_ChannelMaps;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_ChannelMaps(
CREATIONDATE                INT,
ID                          INT,
NAME_DE                     STRING,
TITLE                       STRING,
LIVECHANNELID               INT,
QUALITY                     INT,
REGIONID                    INT,
REGIONNAME                  STRING,
REGIONGEOGRAPHICAREACODE    INT,
TECHNOLOGIESID              INT,
TECHNOLOGIESNAME            STRING,
TECHNOLOGIESTITLE           STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;