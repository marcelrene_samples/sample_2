-- Tabla stage
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_LiveChannels;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_LiveChannels(
CREATIONDATE                STRING,          
ID                          STRING,
NAME_DE                     STRING,
CALLLETTER                  STRING,
EPGLIVECHANNELREFERENCEID   STRING,
ACTIVE                      STRING,
PPV                         STRING,
DVR                         STRING,
TIMESHIFT                   STRING,
NUMBER_NR                   STRING,
REQUIRESPIN                 STRING,
SOURCE                      STRING,
DEVICETYPE                  STRING,
ISVISIBLE                   STRING,
ISPLAYABLE                  STRING,
QUALITY                     STRING,
CONTENTSELECTIONID          STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_LiveChannels/Stage/" 
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/LiveChannels/
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_LiveChannels;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_LiveChannels(
CREATIONDATE                INT,             
ID                          INT,
NAME_DE                     STRING,
CALLLETTER                  STRING,
EPGLIVECHANNELREFERENCEID   INT,
ACTIVE                      BOOLEAN,
PPV                         BOOLEAN,
DVR                         BOOLEAN,
TIMESHIFT                   BOOLEAN,
NUMBER_NR                   INT,
REQUIRESPIN                 BOOLEAN,
SOURCE                      INT,
DEVICETYPE                  INT,
ISVISIBLE                   BOOLEAN,
ISPLAYABLE                  BOOLEAN,
QUALITY                     INT,
CONTENTSELECTIONID          INT,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;