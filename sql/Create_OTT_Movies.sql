-- Tabla stage */
set tez.queue.name=${TEZ_QUEUE};
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_Movies;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_Movies(
ID_ID                           String
,PROVIDER_DE                    String
,TITLE_DE                       String
,STATUS_DE                      String
,LICENSESTART_NR                String
,LICENSEEND_NR                  String
,STATUSDATE_NR                  String
,DEVICES_DE                     String
,DISTRIBUTOR_DE                 String
,DISTRIBUTORID_DE               String
,PRODUCER_DE                    String
,PRODUCERID_DE                  String
,GENRES_DE                      String
,GENRESID_DE                    String
,COMMERCIALIZATIONTYPE_NR	    String
,CONTENTCATEGORYID_NR           String
,TVODTYPEID_NR                  String
,MOVIETYPE_DE                   String
,ORIGINALTITLE_DE               String
,LANGUAGE_DE                    String
,SUBTITLES_DE                   String
,COUNTRY_DE                     String
,RELEASEDATE_NR                 String
,DURATION_NR                    String
,VIEWS_NR                       String
,REQUIRESPIN_DE                 String 
,STARS_NR                       String
,SEASONID_NR                    String
,SERIESID_NR                    String
,ORDER_NR                       String
,TRAILER_DE                     String
,ACTORS_DE                      String
,ACTORSID_DE                    String
,DIRECTORS_DE                   String
,DIRECTORSID_DE                 String
,AGERATINGID_NR                 String
,AGERATINGNAME_DE               String
,CATEGORY_DE                    String
,BILLINGID_DE                   String
,AWARDS_DE                      String
,PRICES_DE                      String
,AVAILABILITYWINDOWSTART_NR     String
,AVAILABILITYWINDOWEND_NR       String
,FILENAME_DE                    String
,IS_DTP_DE                      String
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_Movies/Stage/" 
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/Movies/
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_Movies;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_Movies(
ID_ID                           Int
,PROVIDER_DE                    String
,TITLE_DE                       String
,STATUS_DE                      String
,LICENSESTART_NR                Int
,LICENSEEND_NR                  Int
,STATUSDATE_NR                  Int
,DEVICES_DE                     String
,DISTRIBUTOR_DE                 String
,DISTRIBUTORID_DE               String
,PRODUCER_DE                    String
,PRODUCERID_DE                  String
,GENRES_DE                      String
,GENRESID_DE                    String
,COMMERCIALIZATIONTYPE_NR	    Int
,CONTENTCATEGORYID_NR           Int
,TVODTYPEID_NR                  Int
,MOVIETYPE_DE                   String
,ORIGINALTITLE_DE               String
,LANGUAGE_DE                    String
,SUBTITLES_DE                   String
,COUNTRY_DE                     String
,RELEASEDATE_NR                 Int
,DURATION_NR                    Int
,VIEWS_NR                       Int
,REQUIRESPIN_DE                 Boolean 
,STARS_NR                       Float
,SEASONID_NR                    Int
,SERIESID_NR                    Int
,ORDER_NR                       Int
,TRAILER_DE                     Boolean
,ACTORS_DE                      String
,ACTORSID_DE                    String
,DIRECTORS_DE                   String
,DIRECTORSID_DE                 String
,AGERATINGID_NR                 Int
,AGERATINGNAME_DE               String
,CATEGORY_DE                    String
,BILLINGID_DE                   String
,AWARDS_DE                      String
,PRICES_DE                      String
,AVAILABILITYWINDOWSTART_NR     Int
,AVAILABILITYWINDOWEND_NR       Int
,FILENAME_DE                    String
,IS_DTP_DE                      Boolean
,ORIGIN_FILE_NAME               STRING
,LICENSESTART_ARG_FH            TIMESTAMP
,LICENSEEND_ARG_FH              TIMESTAMP
,RELEASEDATE_ARG_FH             TIMESTAMP
,AVAILABILITYWINDOWSTART_ARG_FH TIMESTAMP
,AVAILABILITYWINDOWEND_ARG_FH   TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;