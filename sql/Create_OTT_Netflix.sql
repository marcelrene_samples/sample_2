-- Tabla stage
DROP TABLE IF EXISTS trafico.ext_gvp_ott_netflix;
CREATE EXTERNAL TABLE trafico.ext_gvp_ott_netflix(
userid STRING
,unique_user_id STRING
,device_id STRING
,time_stamp STRING
,netflix_type STRING
,duration STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_Netflix/Stage/" 
TBLPROPERTIES ("skip.header.line.count"="1");

DROP TABLE IF EXISTS trafico.gvp_ott_netflix_fp;
CREATE TABLE IF NOT EXISTS trafico.gvp_ott_netflix_fp(
userid varchar(8) 
,unique_user_id varchar(50)
,device_id varchar(10)
,time_stamp TIMESTAMP
,netflix_type varchar(7)
,duration varchar(8)
,ORIGIN_FILE_NAME STRING
,fecha_archivo string
,fecha_ingesta timestamp
,day INT
) PARTITIONED BY (fecha_proceso INT)
STORED AS ORC;

-- Tabla con datos crudos desde GVP/Argentina/OTT/Youbora/
DROP TABLE IF EXISTS trafico.gvp_ott_netflix;
CREATE TABLE IF NOT EXISTS trafico.gvp_ott_netflix(
userid varchar(8) 
,unique_user_id varchar(50)
,device_id varchar(10)
,time_stamp TIMESTAMP
,netflix_type varchar(7)
,duration varchar(8)
,ORIGIN_FILE_NAME STRING
,fecha_archivo string
,fecha_ingesta timestamp
) PARTITIONED BY (day INT)
STORED AS ORC;