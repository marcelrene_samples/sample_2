-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_Playbacks;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_Playbacks(
DATETIME_DT	            String,
PRODUCTTYPE	            String,
PRODUCTNAME	            String,
PRODUCTID	            String,
PRODUCERNAME            String,
DISTRIBUTORNAME	        String,
USERACCOUNT         	String,
USERUNIQUEID	        String,
SERVICETYPE	            String,
COMMERCIALIZATIONTYPE   String,
ORIGIN_FILE_NAME        String,
DATETIME_FH_AR          String
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_Playbacks/Stage/"
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/Playbacks */
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_Playbacks;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_Playbacks(
DATETIME_DT	            Int,
PRODUCTTYPE	            String,
PRODUCTNAME	            String,
PRODUCTID	            Int,
PRODUCERNAME	        String,
DISTRIBUTORNAME	        String,
USERACCOUNT             String,
USERUNIQUEID	        String,
SERVICETYPE	            String,
COMMERCIALIZATIONTYPE	String,
ORIGIN_FILE_NAME        String,
DATETIME_FH_AR          TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;