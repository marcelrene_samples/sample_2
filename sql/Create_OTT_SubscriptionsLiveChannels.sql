-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_SubscriptionsLiveChannels;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_SubscriptionsLiveChannels(
CREATIONDATE                STRING,
SUBSCRIPTIONID              STRING,
SUBSCRIPTIONNAME            STRING,
LIVECHANNELID               STRING,
LIVECHANNELNAME             STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_SubscriptionsLiveChannels/Stage/" --VERIFICAR LOCATION
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/SubscriptionsLiveChannels/
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_SubscriptionsLiveChannels;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_SubscriptionsLiveChannels(
CREATIONDATE                INT,
SUBSCRIPTIONID              INT,
SUBSCRIPTIONNAME            STRING,
LIVECHANNELID               INT,
LIVECHANNELNAME             STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;