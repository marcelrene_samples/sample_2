-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_UsersPreferences;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_UsersPreferences(
CREATIONDATE                STRING,
ID                          STRING,
OWNER_DE                    STRING,
DEVICETYPEID                STRING,
USERUNIQUEID                STRING,
NAME_DE                     STRING,
PREFERENCENAME              STRING,
VALUE_DE                    STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_UsersPreferences/Stage/" --VERIFICAR LOCATION
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/UsersPreferences/
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_UsersPreferences;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_UsersPreferences(
CREATIONDATE                INT,
ID                          INT,
OWNER_DE                    INT,
DEVICETYPEID                INT,
USERUNIQUEID                STRING,
NAME_DE                     STRING,
PREFERENCENAME              STRING,
VALUE_DE                    STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;