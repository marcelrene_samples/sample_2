-- Tabla stage */
DROP TABLE IF EXISTS TRAFICO.Ext_GVP_OTT_UsersTags;
CREATE EXTERNAL TABLE TRAFICO.Ext_GVP_OTT_UsersTags(
CREATIONDATE                STRING,      
ID                          STRING,
TYPEID                      STRING,
USERUNIQUEID                STRING,
NAME_DE                     STRING,
VALUE_DE                    STRING,
PRODUCTTYPE                 STRING,
PRODUCTID                   STRING,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          STRING
)row format delimited fields terminated by '\073'
LINES TERMINATED BY '\n'
LOCATION "/transferencias/IPTV/crudos/OTT_UsersTags/Stage/" --VERIFICAR LOCATION
TBLPROPERTIES ("skip.header.line.count"="1");

-- Tabla con datos crudos desde GVP/Argentina/OTT/UsersTags/
DROP TABLE IF EXISTS TRAFICO.GVP_OTT_UsersTags;
CREATE TABLE IF NOT EXISTS TRAFICO.GVP_OTT_UsersTags(
CREATIONDATE                INT,      
ID                          INT,
TYPEID                      INT,
USERUNIQUEID                STRING,
NAME_DE                     STRING,
VALUE_DE                    STRING,
PRODUCTTYPE                 STRING,
PRODUCTID                   INT,
ORIGIN_FILE_NAME            STRING,
CREATIONDATE_FH_AR          TIMESTAMP
) PARTITIONED BY (day INT)
STORED AS ORC;