INSERT INTO TRAFICO.GVP_IPTV_ChannelMaps PARTITION (day) 
SELECT 
  a.CREATIONDATE,
  a.ID,
  a.NAME_DE,
  a.TITLE,
  a.LIVECHANNELID,
  a.QUALITY,
  a.REGIONID,
  a.REGIONNAME,
  a.REGIONGEOGRAPHICAREACODE,
  a.TECHNOLOGIESID,
  a.TECHNOLOGIESNAME,
  a.TECHNOLOGIESTITLE,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_IPTV_ChannelMaps_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_ChannelMaps WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.ID as string),'') = nvl(cast(b.ID as string),'') and
  nvl(a.NAME_DE,'') = nvl(b.NAME_DE,'') and
  nvl(a.TITLE,'') = nvl(b.TITLE,'') and
  nvl(cast(a.LIVECHANNELID as string),'') = nvl(cast(b.LIVECHANNELID as string),'') and
  nvl(cast(a.QUALITY as string),'') = nvl(cast(b.QUALITY as string),'') and
  nvl(cast(a.REGIONID as string),'') = nvl(cast(b.REGIONID as string),'') and
  nvl(a.REGIONNAME,'') = nvl(b.REGIONNAME,'') and
  nvl(cast(a.REGIONGEOGRAPHICAREACODE as string),'') = nvl(cast(b.REGIONGEOGRAPHICAREACODE as string),'') and
  nvl(cast(a.TECHNOLOGIESID as string),'') = nvl(cast(b.TECHNOLOGIESID as string),'') and
  nvl(a.TECHNOLOGIESTITLE,'') = nvl(b.TECHNOLOGIESTITLE,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'') 
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.CREATIONDATE is null and
  b.ID is null and
  b.NAME_DE is null and
  b.TITLE is null and
  b.LIVECHANNELID is null and
  b.QUALITY is null and
  b.REGIONID is null and
  b.REGIONNAME is null and
  b.REGIONGEOGRAPHICAREACODE is null and
  b.TECHNOLOGIESID is null and
  b.TECHNOLOGIESTITLE is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null;
  