INSERT INTO TRAFICO.GVP_IPTV_Login PARTITION (day) 
SELECT 
  a.DATETIME_DT,
  a.DEVICETYPEUSED,
  a.DEVICEID,
  a.STATUSMESSAGE,
  a.STATUSCODE,
  a.USERTYPE, 
  a.UNIQUEUSERCODE,
  a.METHOD,
  a.SESSIONID,
  a.USERID,
  a.ORIGIN_FILE_NAME,
  a.DATETIME_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.DAY
FROM TRAFICO.GVP_IPTV_Login_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_Login WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.DATETIME_DT as string),'') = nvl(cast(b.DATETIME_DT as string),'') and
  nvl(cast(a.DEVICETYPEUSED as string),'') = nvl(cast(b.DEVICETYPEUSED as string),'') and
  nvl(cast(a.DEVICEID as string),'') = nvl(cast(b.DEVICEID as string),'') and
  nvl(a.STATUSMESSAGE,'') = nvl(b.STATUSMESSAGE,'') and
  nvl(cast(a.STATUSCODE as string),'') = nvl(cast(b.STATUSCODE as string),'') and
  nvl(cast(a.USERTYPE as string),'') = nvl(cast(b.USERTYPE as string),'') and
  nvl(a.UNIQUEUSERCODE,'') = nvl(b.UNIQUEUSERCODE,'') and
  nvl(cast(a.METHOD as string),'') = nvl(cast(b.METHOD as string),'') and
  nvl(a.SESSIONID,'') = nvl(b.SESSIONID,'') and
  nvl(a.USERID,'') = nvl(b.USERID,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(CAST(a.DATETIME_FH_AR AS STRING),'') = nvl(CAST(b.DATETIME_FH_AR AS STRING),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.DATETIME_DT is null and
  b.DEVICETYPEUSED is null and
  b.DEVICEID is null and
  b.STATUSMESSAGE is null and
  b.STATUSCODE is null and
  b.USERTYPE is null and
  b.UNIQUEUSERCODE is null and
  b.METHOD is null and
  b.SESSIONID is null and
  b.USERID is null and
  b.ORIGIN_FILE_NAME is null and
  b.DATETIME_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null 
;
