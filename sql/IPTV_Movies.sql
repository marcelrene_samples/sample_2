INSERT INTO TRAFICO.GVP_IPTV_Movies PARTITION (day) 
SELECT 
  a.ID_ID
  ,a.PROVIDER_DE
  ,a.TITLE_DE
  ,a.STATUS_DE
  ,a.LICENSESTART_NR
  ,a.LICENSEEND_NR
  ,a.STATUSDATE_NR
  ,a.DEVICES_DE
  ,a.DISTRIBUTOR_DE
  ,a.DISTRIBUTORID_DE
  ,a.PRODUCER_DE
  ,a.PRODUCERID_DE
  ,a.GENRES_DE
  ,a.GENRESID_DE
  ,a.COMMERCIALIZATIONTYPE_NR
  ,a.CONTENTCATEGORYID_NR
  ,a.TVODTYPEID_NR
  ,a.MOVIETYPE_DE
  ,a.ORIGINALTITLE_DE
  ,a.LANGUAGE_DE
  ,a.SUBTITLES_DE
  ,a.COUNTRY_DE
  ,a.RELEASEDATE_NR
  ,a.DURATION_NR
  ,a.VIEWS_NR
  ,a.REQUIRESPIN_DE
  ,a.STARS_NR
  ,a.SEASONID_NR
  ,a.SERIESID_NR
  ,a.ORDER_NR
  ,a.TRAILER_DE
  ,a.ACTORS_DE
  ,a.ACTORSID_DE
  ,a.DIRECTORS_DE
  ,a.DIRECTORSID_DE
  ,a.AGERATINGID_NR 
  ,a.AGERATINGNAME_DE
  ,a.CATEGORY_DE
  ,a.BILLINGID_DE
  ,a.AWARDS_DE
  ,a.PRICES_DE
  ,a.AVAILABILITYWINDOWSTART_NR
  ,a.AVAILABILITYWINDOWEND_NR
  ,a.FILENAME_DE
  ,a.IS_DTP_DE
  ,a.ORIGIN_FILE_NAME
  ,a.LICENSESTART_ARG_FH
  ,a.LICENSEEND_ARG_FH
  ,a.RELEASEDATE_ARG_FH
  ,a.AVAILABILITYWINDOWSTART_ARG_FH
  ,a.AVAILABILITYWINDOWEND_ARG_FH
  ,a.fecha_archivo
  ,a.fecha_ingesta
  ,a.day
FROM TRAFICO.GVP_IPTV_Movies_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_Movies WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.ID_ID as string),'')  = nvl(cast(b.ID_ID as string),'') and
  nvl(a.PROVIDER_DE,'')  = nvl(b.PROVIDER_DE,'')  and
  nvl(a.TITLE_DE,'')  = nvl(b.TITLE_DE,'')  and
  nvl(a.STATUS_DE,'')  = nvl(b.STATUS_DE,'')  and
  nvl(cast(a.LICENSESTART_NR as string),'')  = nvl(cast(b.LICENSESTART_NR as string),'')  and
  nvl(cast(a.LICENSEEND_NR as string),'')  = nvl(cast(b.LICENSEEND_NR as string),'')  and
  nvl(cast(a.STATUSDATE_NR as string),'')  = nvl(cast(b.STATUSDATE_NR as string),'')  and
  nvl(a.DEVICES_DE,'')  = nvl(b.DEVICES_DE,'')  and
  nvl(a.DISTRIBUTOR_DE,'')  = nvl(b.DISTRIBUTOR_DE,'')  and
  nvl(a.DISTRIBUTORID_DE,'')  = nvl(b.DISTRIBUTORID_DE,'')  and
  nvl(a.PRODUCER_DE,'')  = nvl(b.PRODUCER_DE,'')  and
  nvl(a.PRODUCERID_DE,'')  = nvl(b.PRODUCERID_DE,'')  and
  nvl(a.GENRES_DE,'')  = nvl(b.GENRES_DE,'')  and
  nvl(a.GENRESID_DE,'')  = nvl(b.GENRESID_DE,'')  and
  nvl(cast(a.COMMERCIALIZATIONTYPE_NR as string),'')  = nvl(cast(b.COMMERCIALIZATIONTYPE_NR as string),'')  and
  nvl(cast(a.CONTENTCATEGORYID_NR as string),'')   = nvl(cast(b.CONTENTCATEGORYID_NR as string),'')  and
  nvl(cast(a.TVODTYPEID_NR as string),'')  = nvl(cast(b.TVODTYPEID_NR as string),'')  and
  nvl(a.MOVIETYPE_DE,'')  = nvl(b.MOVIETYPE_DE,'')  and
  nvl(a.ORIGINALTITLE_DE,'')  = nvl(b.ORIGINALTITLE_DE,'')  and
  nvl(a.LANGUAGE_DE,'')  = nvl(b.LANGUAGE_DE,'')  and
  nvl(a.SUBTITLES_DE,'')  = nvl(b.SUBTITLES_DE,'')  and
  nvl(a.COUNTRY_DE,'')  = nvl(b.COUNTRY_DE,'')  and
  nvl(cast(a.RELEASEDATE_NR as string),'')  = nvl(cast(b.RELEASEDATE_NR as string),'')  and
  nvl(cast(a.DURATION_NR as string),'')  = nvl(cast(b.DURATION_NR as string),'')  and
  nvl(cast(a.VIEWS_NR as string),'')  = nvl(cast(b.VIEWS_NR as string),'')  and
  nvl(cast(a.REQUIRESPIN_DE as string),'')  = nvl(cast(b.REQUIRESPIN_DE as string),'')  and
  nvl(cast(a.STARS_NR as string),'')  = nvl(cast(b.STARS_NR as string),'')  and
  nvl(cast(a.SEASONID_NR as string),'')  = nvl(cast(b.SEASONID_NR as string),'')  and
  nvl(cast(a.SERIESID_NR as string),'')  = nvl(cast(b.SERIESID_NR as string),'')  and
  nvl(cast(a.ORDER_NR as string),'')  = nvl(cast(b.ORDER_NR as string),'')  and
  nvl(cast(a.TRAILER_DE as string),'')  = nvl(cast(b.TRAILER_DE as string),'')  and
  nvl(a.ACTORS_DE,'')  = nvl(b.ACTORS_DE,'')  and
  nvl(a.ACTORSID_DE,'')  = nvl(b.ACTORSID_DE,'')  and
  nvl(a.DIRECTORS_DE,'')  = nvl(b.DIRECTORS_DE,'')  and
  nvl(a.DIRECTORSID_DE,'')  = nvl(b.DIRECTORSID_DE,'')  and
  nvl(cast(a.AGERATINGID_NR as string),'')  = nvl(cast(b.AGERATINGID_NR as string),'')  and
  nvl(a.AGERATINGNAME_DE,'')  = nvl(b.AGERATINGNAME_DE,'')  and
  nvl(a.CATEGORY_DE,'')  = nvl(b.CATEGORY_DE,'')  and
  nvl(a.BILLINGID_DE,'')  = nvl(b.BILLINGID_DE,'')  and
  nvl(a.AWARDS_DE,'')  = nvl(b.AWARDS_DE,'')  and
  nvl(a.PRICES_DE,'')  = nvl(b.PRICES_DE,'')  and
  nvl(cast(a.AVAILABILITYWINDOWSTART_NR as string),'')  = nvl(cast(b.AVAILABILITYWINDOWSTART_NR as string),'')  and
  nvl(cast(a.AVAILABILITYWINDOWEND_NR as string),'')  = nvl(cast(b.AVAILABILITYWINDOWEND_NR as string),'')  and 
  nvl(a.FILENAME_DE,'')  = nvl(b.FILENAME_DE,'')  and
  nvl(cast(a.IS_DTP_DE as string),'')  = nvl(cast(b.IS_DTP_DE as string),'')  and
  nvl(a.ORIGIN_FILE_NAME,'')  = nvl(b.ORIGIN_FILE_NAME,'')  and
  nvl(cast(a.LICENSESTART_ARG_FH as string),'')  = nvl(cast(b.LICENSESTART_ARG_FH as string),'')  and
  nvl(cast(a.LICENSEEND_ARG_FH as string),'')  = nvl(cast(b.LICENSEEND_ARG_FH as string),'')  and
  nvl(cast(a.RELEASEDATE_ARG_FH as string),'')  = nvl(cast(b.RELEASEDATE_ARG_FH as string),'')  and
  nvl(cast(a.AVAILABILITYWINDOWSTART_ARG_FH as string),'')  = nvl(cast(b.AVAILABILITYWINDOWSTART_ARG_FH as string),'')  and
  nvl(cast(a.AVAILABILITYWINDOWEND_ARG_FH as string),'')  = nvl(cast(b.AVAILABILITYWINDOWEND_ARG_FH as string),'')  and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.ID_ID is null and
  b.PROVIDER_DE is null and 
  b.TITLE_DE is null and
  b.STATUS_DE is null and
  b.LICENSESTART_NR is null and
  b.LICENSEEND_NR is null and
  b.STATUSDATE_NR is null and
  b.DEVICES_DE is null and
  b.DISTRIBUTOR_DE is null and
  b.DISTRIBUTORID_DE is null and
  b.PRODUCER_DE is null and
  b.PRODUCERID_DE is null and
  b.GENRES_DE is null and
  b.GENRESID_DE is null and
  b.COMMERCIALIZATIONTYPE_NR is null and
  b.CONTENTCATEGORYID_NR is null and
  b.TVODTYPEID_NR is null and
  b.MOVIETYPE_DE is null and
  b.ORIGINALTITLE_DE is null and
  b.LANGUAGE_DE is null and
  b.SUBTITLES_DE is null and
  b.COUNTRY_DE is null and
  b.RELEASEDATE_NR is null and
  b.DURATION_NR is null and
  b.VIEWS_NR is null and
  b.REQUIRESPIN_DE is null and
  b.STARS_NR is null and
  b.SEASONID_NR is null and
  b.SERIESID_NR is null and
  b.ORDER_NR is null and
  b.TRAILER_DE is null and
  b.ACTORS_DE is null and
  b.ACTORSID_DE is null and
  b.DIRECTORS_DE is null and
  b.DIRECTORSID_DE is null and
  b.AGERATINGID_NR is null and
  b.AGERATINGNAME_DE is null and
  b.CATEGORY_DE is null and
  b.BILLINGID_DE is null and
  b.AWARDS_DE is null and
  b.PRICES_DE is null and
  b.AVAILABILITYWINDOWSTART_NR is null and
  b.AVAILABILITYWINDOWEND_NR is null and
  b.FILENAME_DE is null and
  b.IS_DTP_DE is null and
  b.ORIGIN_FILE_NAME is null and
  b.LICENSESTART_ARG_FH is null and
  b.LICENSEEND_ARG_FH is null and
  b.RELEASEDATE_ARG_FH is null and
  b.AVAILABILITYWINDOWSTART_ARG_FH is null and
  b.AVAILABILITYWINDOWEND_ARG_FH is null and
  b.fecha_archivo is null and
  b.fecha_ingesta is null and
  b.day is null;
  
  