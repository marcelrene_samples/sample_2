INSERT INTO TRAFICO.GVP_IPTV_Playbacks PARTITION (day)
SELECT
  a.DATETIME_DT,
  a.PRODUCTTYPE,
  a.PRODUCTNAME,
  a.PRODUCTID,
  a.PRODUCERNAME,
  a.DISTRIBUTORNAME,
  a.USERACCOUNT,
  a.USERUNIQUEID,
  a.SERVICETYPE,
  a.COMMERCIALIZATIONTYPE,
  a.ORIGIN_FILE_NAME,
  a.DATETIME_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.DAY
FROM TRAFICO.GVP_IPTV_Playbacks_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_Playbacks WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.DATETIME_DT as string),'') = nvl(cast(b.DATETIME_DT as string),'') and
  nvl(a.PRODUCTTYPE,'') = nvl(b.PRODUCTTYPE,'') and
  nvl(a.PRODUCTNAME,'') = nvl(b.PRODUCTNAME,'') and
  nvl(cast(a.PRODUCTID as string),'') = nvl(cast(b.PRODUCTID as string),'') and
  nvl(a.PRODUCERNAME,'') = nvl(b.PRODUCERNAME,'') and
  nvl(a.DISTRIBUTORNAME,'') = nvl(b.DISTRIBUTORNAME,'') and
  nvl(a.USERACCOUNT,'') = nvl(b.USERACCOUNT,'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.SERVICETYPE,'') = nvl(b.SERVICETYPE,'') and
  nvl(a.COMMERCIALIZATIONTYPE,'') = nvl(b.COMMERCIALIZATIONTYPE,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.DATETIME_FH_AR as string),'') = nvl(cast(b.DATETIME_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.FECHA_INGESTA>=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.DATETIME_DT is null and
  b.PRODUCTTYPE is null and
  b.PRODUCTNAME is null and
  b.PRODUCTID is null and
  b.PRODUCERNAME is null and
  b.DISTRIBUTORNAME is null and
  b.USERACCOUNT is null and
  b.USERUNIQUEID is null and
  b.SERVICETYPE is null and
  b.COMMERCIALIZATIONTYPE is null and
  b.ORIGIN_FILE_NAME is null and
  b.DATETIME_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null;
  