INSERT INTO TRAFICO.GVP_IPTV_Products PARTITION (day) 
SELECT 
  a.CREATIONDATE,
  a.ID,
  a.NAME_DE,
  a.TYPE_DE,
  a.COMMERCIALIZATIONSTART,
  a.COMMERCIALIZATIONEND,
  a.STATUS_NR,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.COMMERCIALIZATIONSTART_FH_AR,
  a.COMMERCIALIZATIONEND_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_IPTV_Products_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_Products WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.ID as string),'') = nvl(cast(b.ID as string),'') and
  nvl(a.NAME_DE,'') = nvl(b.NAME_DE,'') and
  nvl(a.TYPE_DE,'') = nvl(b.TYPE_DE,'') and
  nvl(cast(a.COMMERCIALIZATIONSTART as string),'') = nvl(cast(b.COMMERCIALIZATIONSTART as string),'') and
  nvl(cast(a.COMMERCIALIZATIONEND as string),'') = nvl(cast(b.COMMERCIALIZATIONEND as string),'') and
  nvl(cast(a.STATUS_NR as string),'') = nvl(cast(b.STATUS_NR as string),'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(cast(a.COMMERCIALIZATIONSTART_FH_AR as string),'') = nvl(cast(b.COMMERCIALIZATIONSTART_FH_AR as string),'') and
  nvl(cast(a.COMMERCIALIZATIONEND_FH_AR as string),'') = nvl(cast(b.COMMERCIALIZATIONEND_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.ID is null and
  b.NAME_DE is null and
  b.TYPE_DE is null and
  b.COMMERCIALIZATIONSTART is null and
  b.COMMERCIALIZATIONEND is null and
  b.STATUS_NR is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.COMMERCIALIZATIONSTART_FH_AR is null and
  b.COMMERCIALIZATIONEND_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null 
  ;
