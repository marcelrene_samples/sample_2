INSERT INTO TRAFICO.GVP_IPTV_Purchases  PARTITION (day)
SELECT
  a.DATETIME_DT,
  a.PRODUCTTYPE,
  a.PRODUCTID,
  a.PRODUCT,
  a.PRODUCTNAME,
  a.PRODUCERNAME,
  a.DISTRIBUTORNAME,
  a.COMMERCIALIZATIONTYPE,
  a.CONTENTCATEGORYID,
  a.TVODTYPEID,
  a.QUALITY,
  a.PRICE,
  a.USEREMAIL,
  a.USERUNIQUEID,
  a.FIRSTNAME,
  a.LASTNAME,
  a.SERVICETYPE,
  a.DEVICETYPENAME,
  a.PAYMENTTYPE,
  a.WEBPAYTRANSACTIONID,
  a.IDWEBPAY,
  a.AUTHWEBPAYCODE,
  a.PURCHASESTATUS,
  a.TRANSACTIONHASH,
  a.SYNCMESSAGE,
  a.DEVICEID,
  a.BILLINGID,
  a.ORIGIN_FILE_NAME,
  a.DATETIME_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.DAY
FROM TRAFICO.GVP_IPTV_Purchases_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_Purchases WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.DATETIME_DT as string),'') = nvl(cast(b.DATETIME_DT as string),'') and
  nvl(a.PRODUCTTYPE,'') = nvl(b.PRODUCTTYPE,'') and
  nvl(cast(a.PRODUCTID as string),'') = nvl(cast(b.PRODUCTID as string),'') and
  nvl(cast(a.PRODUCT as string),'') = nvl(cast(b.PRODUCT as string),'') and
  nvl(a.PRODUCTNAME,'') = nvl(b.PRODUCTNAME,'') and
  nvl(a.PRODUCERNAME,'') = nvl(b.PRODUCERNAME,'') and
  nvl(a.DISTRIBUTORNAME,'') = nvl(b.DISTRIBUTORNAME,'') and
  nvl(cast(a.COMMERCIALIZATIONTYPE as string),'') = nvl(cast(b.COMMERCIALIZATIONTYPE as string),'') and
  nvl(cast(a.CONTENTCATEGORYID as string),'') = nvl(cast(b.CONTENTCATEGORYID as string),'') and
  nvl(cast(a.TVODTYPEID as string),'') = nvl(cast(b.TVODTYPEID as string),'') and
  nvl(a.QUALITY,'') = nvl(b.QUALITY,'') and
  nvl(cast(a.PRICE as string),'') = nvl(cast(b.PRICE as string),'') and 
  nvl(a.USEREMAIL,'') = nvl(b.USEREMAIL,'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.FIRSTNAME,'') = nvl(b.FIRSTNAME,'') and
  nvl(a.LASTNAME,'') = nvl(b.LASTNAME,'') and
  nvl(a.SERVICETYPE,'') = nvl(b.SERVICETYPE,'') and
  nvl(a.DEVICETYPENAME,'') = nvl(b.DEVICETYPENAME,'') and
  nvl(a.PAYMENTTYPE,'') = nvl(b.PAYMENTTYPE,'') and
  nvl(a.WEBPAYTRANSACTIONID,'') = nvl(b.WEBPAYTRANSACTIONID,'') and
  nvl(a.IDWEBPAY,'') = nvl(b.IDWEBPAY,'') and
  nvl(a.AUTHWEBPAYCODE,'') = nvl(b.AUTHWEBPAYCODE,'') and
  nvl(cast(a.PURCHASESTATUS as string),'') = nvl(cast(b.PURCHASESTATUS as string),'')  and
  nvl(a.TRANSACTIONHASH,'') = nvl(b.TRANSACTIONHASH,'') and
  nvl(a.SYNCMESSAGE,'') = nvl(b.SYNCMESSAGE,'') and
  nvl(a.DEVICEID,'') = nvl(b.DEVICEID,'') and
  nvl(a.BILLINGID,'') = nvl(b.BILLINGID,'')  and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.DATETIME_FH_AR as string),'') = nvl(cast(b.DATETIME_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'') 
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.DATETIME_DT is null and
  b.PRODUCTTYPE is null and
  b.PRODUCTID is null and
  b.PRODUCT is null and
  b.PRODUCTNAME is null and
  b.PRODUCERNAME is null and
  b.DISTRIBUTORNAME is null and
  b.COMMERCIALIZATIONTYPE is null and
  b.CONTENTCATEGORYID is null and
  b.TVODTYPEID is null and
  b.QUALITY is null and
  b.PRICE is null and 
  b.USEREMAIL is null and
  b.USERUNIQUEID is null and
  b.FIRSTNAME is null and
  b.LASTNAME is null and
  b.SERVICETYPE is null and
  b.DEVICETYPENAME is null and
  b.PAYMENTTYPE is null and
  b.WEBPAYTRANSACTIONID is null and
  b.IDWEBPAY is null and
  b.AUTHWEBPAYCODE is null and
  b.PURCHASESTATUS is null and
  b.TRANSACTIONHASH is null and
  b.SYNCMESSAGE is null and
  b.DEVICEID is null and
  b.BILLINGID is null  and
  b.ORIGIN_FILE_NAME is null and
  b.DATETIME_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null ;

