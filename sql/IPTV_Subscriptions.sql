INSERT INTO TRAFICO.GVP_IPTV_Subscriptions PARTITION (day) 
SELECT 
  a.CREATIONDATE,                
  a.USERID,                      
  a.USERUNIQUEID,                
  a.USEREMAIL,                   
  a.STATUS_DE,                   
  a.SUBSCRIPTIONID,              
  a.SUBSCRIPTIONNAME,            
  a.PAYMENTTYPE,                 
  a.DATESTART,                   
  a.DATEEND,                     
  a.ORIGIN_FILE_NAME,            
  a.CREATIONDATE_FH_AR,          
  a.DATESTART_FH_AR,            
  a.DATEEND_FH_AR,              
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_IPTV_Subscriptions_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_Subscriptions WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(a.USERID,'') = nvl(b.USERID,'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.USEREMAIL,'') = nvl(b.USEREMAIL,'') and
  nvl(a.STATUS_DE,'') = nvl(b.STATUS_DE,'') and
  nvl(cast(a.SUBSCRIPTIONID as string),'') = nvl(cast(b.SUBSCRIPTIONID as string),'') and
  nvl(a.SUBSCRIPTIONNAME,'') = nvl(b.SUBSCRIPTIONNAME,'') and
  nvl(cast(a.PAYMENTTYPE as string),'') = nvl(cast(b.PAYMENTTYPE as string),'') and
  nvl(cast(a.DATESTART as string),'') = nvl(cast(b.DATESTART as string),'') and
  nvl(cast(a.DATEEND as string),'') = nvl(cast(b.DATEEND as string),'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(cast(a.DATESTART_FH_AR as string),'') = nvl(cast(b.DATESTART_FH_AR as string),'') and
  nvl(cast(a.DATEEND_FH_AR as string),'') = nvl(cast(b.DATEEND_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and 
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.USERID is null and
  b.USERUNIQUEID is null and
  b.USEREMAIL is null and
  b.STATUS_DE is null and
  b.SUBSCRIPTIONID is null and
  b.SUBSCRIPTIONNAME is null and
  b.PAYMENTTYPE is null and
  b.DATESTART is null and
  b.DATEEND is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.DATESTART_FH_AR is null and
  b.DATEEND_FH_AR is null and
  b.fecha_archivo is null and 
  b.day is null ;
  