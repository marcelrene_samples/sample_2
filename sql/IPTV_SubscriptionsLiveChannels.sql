INSERT INTO TRAFICO.GVP_IPTV_SubscriptionsLiveChannels PARTITION (day) 
SELECT 
  a.CREATIONDATE,
  a.SUBSCRIPTIONID,
  a.SUBSCRIPTIONNAME,
  a.LIVECHANNELID,
  a.LIVECHANNELNAME,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,                   
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_IPTV_SubscriptionsLiveChannels_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_SubscriptionsLiveChannels WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.SUBSCRIPTIONID as string),'') = nvl(cast(b.SUBSCRIPTIONID as string),'') and
  nvl(a.SUBSCRIPTIONNAME,'') = nvl(b.SUBSCRIPTIONNAME,'') and
  nvl(cast(a.LIVECHANNELID as string),'') = nvl(cast(b.LIVECHANNELID as string),'') and
  nvl(a.LIVECHANNELNAME,'') = nvl(b.LIVECHANNELNAME,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.SUBSCRIPTIONID is null and
  b.SUBSCRIPTIONNAME is null and
  b.LIVECHANNELID is null and
  b.LIVECHANNELNAME is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null ;
  
  