INSERT INTO TRAFICO.GVP_IPTV_UsersPreferences PARTITION (day) 
SELECT 
  a.CREATIONDATE,
  a.ID,
  a.OWNER_DE,
  a.DEVICETYPEID,
  a.USERUNIQUEID,
  a.NAME_DE,
  a.PREFERENCENAME,
  a.VALUE_DE,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_IPTV_UsersPreferences_fp a 
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_IPTV_UsersPreferences WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.ID as string),'') = nvl(cast(b.ID as string),'') and
  nvl(cast(a.OWNER_DE as string),'') = nvl(cast(b.OWNER_DE as string),'') and
  nvl(cast(a.DEVICETYPEID as string),'') = nvl(cast(b.DEVICETYPEID as string),'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.NAME_DE,'') = nvl(b.NAME_DE,'') and
  nvl(a.PREFERENCENAME,'') = nvl(b.PREFERENCENAME,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and  
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.ID is null and
  b.OWNER_DE is null and
  b.DEVICETYPEID is null and
  b.USERUNIQUEID is null and
  b.NAME_DE is null and
  b.PREFERENCENAME is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and  
  b.day is null;

