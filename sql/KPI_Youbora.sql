--Carga de la tabla final con transformación de horas a UTC-3 y particionada por 
set tez.queue.name=${TEZ_QUEUE};
INSERT INTO TRAFICO.GVP_KPI_Youbora PARTITION (day)
SELECT 
a.Playtime_CA
,a.Traffic_CA
,a.Seek_count_CA
,a.Effective_Playtime_CA
,a.Pause_Time_CA
,a.Avg_Seek_Time_CA
,a.Join_Time_CA
,a.interruptions_CA
,a.Buffer_Ratio_NR
,a.Buffer_Time_CA
,a.Avg_Bitrate_NR
,a.Happiness_Score_NR
,a.Avg_Buffer_Duration_NR
,a.Playback_Stalls_NR
,a.Unicast_Channel_Change_Mean_Time_NR
,a.Multicast_Channel_Join_Mean_Time_NR
,a.View_Lost_Packets_CA
,a.View_Fixed_Packets_CA
,a.View_Packet_Loss_CA
,a.Lost_Bursts_CA
,a.Fixed_Bursts_CA
,a.Failed_Bursts_CA
,a.View_Avg_RSSI_NR
,a.View_Avg_Physical_Rate_NR
,a.View_Avg_Channel_interference_NR
,a.Startup_Error_Count_CA
,a.In_Stream_Error_Total_CA
,a.Media_Duration_NR
,a.Completion_Rate_NR
,a.End_Time_FH
,a.Start_Time_FH
,a.Play_Head_NR
,a.Country_DE
,a.City_DE
,a.Postal_Code_DE
,a.State_Province_DE
,a.Region_DE
,a.ISP_DE
,a.Connection_Type_DE
,a.CDN_DE
,a.CDN_Node_Host_DE
,a.ASN_NR
,a.Viewing_Mode_DE
,a.Title_DE
,a.Streaming_protocol_DE
,a.Resource_Domain_DE
,a.Type_DE
,a.Domain_DE
,a.Device_DE
,a.Device_Type_data_filter_DE
,a.Device_Vendor_DE
,a.Device_Model_DE
,a.Browser_DE
,a.Browser_Version_DE
,a.OS_DE
,a.OS_Version_DE
,a.Plugin_Version_DE
,a.Player_DE
,a.Player_Version_DE
,a.Token_DE
,a.Metadata_DE
,a.Error_Metadata_DE
,a.Rendition_DE
,a.Error_Name_DE
,a.Error_Description_DE
,a.Crash_Status_DE
,a.User_ID_NR
,a.Transaction_ID_DE
,a.IP_DE
,a.IP_Version_DE
,a.NUMBER_INSTANCE_DE
,a.SELECTED_QUALITY_DE
,a.CODE_INSTANCE_DE
,a.DEVICE_TYPE_DE
,a.VERSION_DE
,a.CONNECTION_DE
,a.COMMERCIALIZATION_TYPE_DE
,a.Dimension_8_DE
,a.Dimension_9_DE
,a.Dimension_10_DE
,a.QOS_Network_DE
,a.QOS_TV_Model_DE
,a.QOS_Origin_DE
,a.QOS_Device_DE
,a.QOS_MAC_Address_DE
,a.QOS_Topology_Level1_DE
,a.QOS_Topology_Level2_DE
,a.QOS_Topology_Level3_DE
,a.QOS_Topology_Level4_DE
,a.QOS_Topology_Level5_DE
,a.QOS_Topology_Level1_Type_Name_DE
,a.QOS_Topology_Level2_Type_Name_DE
,a.QOS_Topology_Level3_Type_Name_DE
,a.QOS_Topology_Level4_Type_Name_DE
,a.QOS_Topology_Level5_Type_Name_DE
,a.QOS_Topology_Level2_Slot_DE
,a.QOS_Topology_Level2_Port_DE
,a.QOS_Topology_Level3_Slot_DE
,a.QOS_Topology_Level3_Port_DE
,a.QOS_Topology_Level4_Slot_DE
,a.QOS_Topology_Level4_Port_DE
,a.QOS_Topology_Level5_Slot_DE
,a.QOS_Topology_Level5_Port_DE
,a.QOS_Boot_ROM_version_DE
,a.QOS_HDMI_Format_DE
,a.QOS_Local_PVR_DE
,a.QOS_Software_version_DE
,a.QOS_Unique_user_DE
,a.QOS_Content_type_DE
,a.QOS_Certification_DE
,a.QOS_Device_Error_Code_DE
,a.QOS_Device_Error_Message_DE
,a.QOS_Geographic_Area_DE
,a.QOS_HGU_Model_DE
,a.QOS_STB_Model_DE
,a.QOS_Multicast_Group_DE
,a.ORIGIN_FILE_NAME_DE
,a.fecha_archivo
,a.fecha_ingesta
,a.Latitude_de
,a.Longitude_de
,a.CDN_Request_Type_de
,a.Media_Resource_de
,a.Playback_Status_de
,a.day
FROM TRAFICO.GVP_KPI_Youbora_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_KPI_Youbora WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.Playtime_CA as string),'') = nvl(cast(b.Playtime_CA as string),'') and 
  nvl(cast(a.Traffic_CA as string),'')=nvl(cast(b.Traffic_CA as string),'') and 
  nvl(cast(a.Seek_count_CA as string),'')=nvl(cast(b.Seek_count_CA as string),'') and 
  nvl(cast(a.Effective_Playtime_CA as string),'')=nvl(cast(b.Effective_Playtime_CA as string),'') and 
  nvl(cast(a.Pause_Time_CA as string),'')=nvl(cast(b.Pause_Time_CA as string),'') and 
  nvl(cast(a.Avg_Seek_Time_CA as string),'')=nvl(cast(b.Avg_Seek_Time_CA as string),'') and 
  nvl(cast(a.Join_Time_CA as string),'')=nvl(cast(b.Join_Time_CA as string),'') and
  nvl(cast(a.interruptions_CA as string),'')=nvl(cast(b.interruptions_CA as string),'') and 
  nvl(cast(a.Buffer_Ratio_NR as string),'')=nvl(cast(b.Buffer_Ratio_NR as string),'') and 
  nvl(cast(a.Buffer_Time_CA as string),'')=nvl(cast(b.Buffer_Time_CA as string),'') and 
  nvl(cast(a.Avg_Bitrate_NR as string),'')=nvl(cast(b.Avg_Bitrate_NR as string),'') and 
  nvl(cast(a.Happiness_Score_NR as string),'')=nvl(cast(b.Happiness_Score_NR as string),'') and 
  nvl(a.Avg_Buffer_Duration_NR,'')= nvl(b.Avg_Buffer_Duration_NR,'') and 
  nvl(a.Playback_Stalls_NR,'')=nvl(b.Playback_Stalls_NR,'') and 
  nvl(cast(a.Unicast_Channel_Change_Mean_Time_NR as string),'')= nvl(cast(b.Unicast_Channel_Change_Mean_Time_NR as string),'') and 
  nvl(cast(a.Multicast_Channel_Join_Mean_Time_NR as string),'')=nvl(cast(b.Multicast_Channel_Join_Mean_Time_NR as string),'') and 
  nvl(cast(a.View_Lost_Packets_CA as string),'')=nvl(cast(b.View_Lost_Packets_CA as string),'') and 
  nvl(cast(a.View_Fixed_Packets_CA as string),'')= nvl(cast(b.View_Fixed_Packets_CA as string),'') and
  nvl(cast(a.View_Packet_Loss_CA as string),'')= nvl(cast(b.View_Packet_Loss_CA as string),'') and 
  nvl(cast(a.Lost_Bursts_CA as string),'')= nvl(cast(b.Lost_Bursts_CA as string),'') and 
  nvl(cast(a.Fixed_Bursts_CA as string),'')=nvl(cast(b.Fixed_Bursts_CA as string),'') and 
  nvl(cast(a.Failed_Bursts_CA as string),'')=nvl(cast(b.Failed_Bursts_CA as string),'') and 
  nvl(cast(a.View_Avg_RSSI_NR as string),'')= nvl(cast(b.View_Avg_RSSI_NR as string),'') and 
  nvl(cast(a.View_Avg_Physical_Rate_NR as string),'')=nvl(cast(b.View_Avg_Physical_Rate_NR as string),'') and 
  nvl(cast(a.View_Avg_Channel_interference_NR as string),'')= nvl(cast(b.View_Avg_Channel_interference_NR as string),'') and 
  nvl(cast(a.Startup_Error_Count_CA as string),'')= nvl(cast(b.Startup_Error_Count_CA as string),'') and 
  nvl(cast(a.In_Stream_Error_Total_CA as string),'')=nvl(cast(b.In_Stream_Error_Total_CA as string),'') and 
  nvl(cast(a.Media_Duration_NR as string),'')=nvl(cast(b.Media_Duration_NR as string),'') and 
  nvl(cast(a.Completion_Rate_NR as string),'')= nvl(cast(b.Completion_Rate_NR as string),'') and 
  nvl(cast(a.End_Time_FH as string),'')=nvl(cast(b.End_Time_FH as string),'') and 
  nvl(cast(a.Start_Time_FH as string),'')=nvl(cast(b.Start_Time_FH as string),'') and 
  nvl(cast(a.Play_Head_NR as string),'')=nvl(cast(b.Play_Head_NR as string),'') and 
  nvl(a.Country_DE,'')=nvl(b.Country_DE,'') and 
  nvl(a.City_DE,'')=nvl(b.City_DE,'') and 
  nvl(a.Postal_Code_DE,'')=nvl(b.Postal_Code_DE,'') and 
  nvl(a.State_Province_DE,'')=nvl(b.State_Province_DE,'') and
  nvl(a.Region_DE,'')=nvl(b.Region_DE,'') and
  nvl(a.ISP_DE,'')=nvl(b.ISP_DE,'') and
  nvl(a.Connection_Type_DE,'')=nvl(b.Connection_Type_DE,'') and
  nvl(a.CDN_DE,'')=nvl(b.CDN_DE,'') and 
  nvl(a.CDN_Node_Host_DE,'')=nvl(b.CDN_Node_Host_DE,'') and
  nvl(cast(a.ASN_NR as string),'')=nvl(cast(b.ASN_NR as string),'') and
  nvl(a.Viewing_Mode_DE,'')=nvl(b.Viewing_Mode_DE,'') and
  nvl(a.Title_DE,'')=nvl(b.Title_DE,'') and
  nvl(a.Streaming_protocol_DE,'')=nvl(b.Streaming_protocol_DE,'') and
  nvl(a.Resource_Domain_DE,'')=nvl(b.Resource_Domain_DE,'') and
  nvl(a.Type_DE,'')=nvl(b.Type_DE,'') and
  nvl(a.Domain_DE,'')=nvl(b.Domain_DE,'') and
  nvl(a.Device_DE,'')=nvl(b.Device_DE,'') and
  nvl(a.Device_Type_data_filter_DE,'')=nvl(b.Device_Type_data_filter_DE,'') and
  nvl(a.Device_Vendor_DE,'')=nvl(b.Device_Vendor_DE,'') and
  nvl(a.Device_Model_DE,'')=nvl(b.Device_Model_DE,'') and
  nvl(a.Browser_DE,'')=nvl(b.Browser_DE,'') and
  nvl(a.Browser_Version_DE,'')=nvl(b.Browser_Version_DE,'') and
  nvl(a.OS_DE,'')=nvl(b.OS_DE,'') and
  nvl(a.OS_Version_DE,'')= nvl(b.OS_Version_DE,'') and
  nvl(a.Plugin_Version_DE,'')=nvl(b.Plugin_Version_DE,'') and
  nvl(a.Player_DE,'')=nvl(b.Player_DE,'') and
  nvl(a.Player_Version_DE,'')=nvl(b.Player_Version_DE,'') and
  nvl(a.Token_DE,'')=nvl(b.Token_DE,'') and
  nvl(a.Metadata_DE,'')=nvl(b.Metadata_DE,'') and
  nvl(a.Error_Metadata_DE,'')=nvl(b.Error_Metadata_DE,'') and
  nvl(a.Rendition_DE,'')=nvl(b.Rendition_DE,'') and
  nvl(a.Error_Name_DE,'')=nvl(b.Error_Name_DE,'') and
  nvl(a.Error_Description_DE,'')=nvl(b.Error_Description_DE,'') and
  nvl(a.Crash_Status_DE,'')=nvl(b.Crash_Status_DE,'') and
  nvl(cast(a.User_ID_NR as string),'')=nvl(cast(b.User_ID_NR as string),'') and
  nvl(a.Transaction_ID_DE,'')=nvl(b.Transaction_ID_DE,'') and
  nvl(a.IP_DE,'')=nvl(b.IP_DE,'') and
  nvl(a.IP_Version_DE,'')=nvl(b.IP_Version_DE,'') and
  nvl(a.NUMBER_INSTANCE_DE,'')=nvl(b.NUMBER_INSTANCE_DE,'') and
  nvl(a.SELECTED_QUALITY_DE,'')=nvl(b.SELECTED_QUALITY_DE,'') and
  nvl(a.CODE_INSTANCE_DE,'')=nvl(b.CODE_INSTANCE_DE,'') and
  nvl(a.DEVICE_TYPE_DE,'')=nvl(b.DEVICE_TYPE_DE,'') and
  nvl(a.VERSION_DE,'')=nvl(b.VERSION_DE,'') and
  nvl(a.CONNECTION_DE,'')=nvl(b.CONNECTION_DE,'') and
  nvl(a.COMMERCIALIZATION_TYPE_DE,'')=nvl(b.COMMERCIALIZATION_TYPE_DE,'') and
  nvl(a.Dimension_8_DE,'')=nvl(b.Dimension_8_DE,'') and
  nvl(a.Dimension_9_DE,'')=nvl(b.Dimension_9_DE,'') and
  nvl(a.Dimension_10_DE,'')=nvl(b.Dimension_10_DE,'') and
  nvl(a.QOS_Network_DE,'')=nvl(b.QOS_Network_DE,'') and
  nvl(a.QOS_TV_Model_DE,'')=nvl(b.QOS_TV_Model_DE,'') and
  nvl(a.QOS_Origin_DE,'')=nvl(b.QOS_Origin_DE,'') and
  nvl(a.QOS_Device_DE,'')=nvl(b.QOS_Device_DE,'') and
  nvl(a.QOS_MAC_Address_DE,'')=nvl(b.QOS_MAC_Address_DE,'') and
  nvl(a.QOS_Topology_Level1_DE,'')=nvl(b.QOS_Topology_Level1_DE,'') and
  nvl(a.QOS_Topology_Level2_DE,'')=nvl(b.QOS_Topology_Level2_DE,'') and
  nvl(a.QOS_Topology_Level3_DE,'')=nvl(b.QOS_Topology_Level3_DE,'') and
  nvl(a.QOS_Topology_Level4_DE,'')=nvl(b.QOS_Topology_Level4_DE,'') and
  nvl(a.QOS_Topology_Level5_DE,'')=nvl(b.QOS_Topology_Level5_DE,'') and
  nvl(a.QOS_Topology_Level1_Type_Name_DE,'')=nvl(b.QOS_Topology_Level1_Type_Name_DE,'') and
  nvl(a.QOS_Topology_Level2_Type_Name_DE,'')=nvl(b.QOS_Topology_Level2_Type_Name_DE,'') and
  nvl(a.QOS_Topology_Level3_Type_Name_DE,'')=nvl(b.QOS_Topology_Level3_Type_Name_DE,'') and
  nvl(a.QOS_Topology_Level4_Type_Name_DE,'')=nvl(b.QOS_Topology_Level4_Type_Name_DE,'') and
  nvl(a.QOS_Topology_Level5_Type_Name_DE,'')=nvl(b.QOS_Topology_Level5_Type_Name_DE,'') and
  nvl(a.QOS_Topology_Level2_Slot_DE,'')=nvl(b.QOS_Topology_Level2_Slot_DE,'') and
  nvl(a.QOS_Topology_Level2_Port_DE,'')=nvl(b.QOS_Topology_Level2_Port_DE,'') and
  nvl(a.QOS_Topology_Level3_Slot_DE,'')=nvl(b.QOS_Topology_Level3_Slot_DE,'') and
  nvl(a.QOS_Topology_Level3_Port_DE,'')=nvl(b.QOS_Topology_Level3_Port_DE,'') and
  nvl(a.QOS_Topology_Level4_Slot_DE,'')=nvl(b.QOS_Topology_Level4_Slot_DE,'') and
  nvl(a.QOS_Topology_Level4_Port_DE,'')=nvl(b.QOS_Topology_Level4_Port_DE,'') and
  nvl(a.QOS_Topology_Level5_Slot_DE,'')=nvl(b.QOS_Topology_Level5_Slot_DE,'') and
  nvl(a.QOS_Topology_Level5_Port_DE,'')=nvl(b.QOS_Topology_Level5_Port_DE,'') and
  nvl(a.QOS_Boot_ROM_version_DE,'')=nvl(b.QOS_Boot_ROM_version_DE,'') and 
  nvl(a.QOS_HDMI_Format_DE,'')=nvl(b.QOS_HDMI_Format_DE,'') and
  nvl(a.QOS_Local_PVR_DE,'')=nvl(b.QOS_Local_PVR_DE,'') and
  nvl(a.QOS_Software_version_DE,'')=nvl(b.QOS_Software_version_DE,'') and
  nvl(a.QOS_Unique_user_DE,'')=nvl(b.QOS_Unique_user_DE,'') and
  nvl(a.QOS_Content_type_DE,'')=nvl(b.QOS_Content_type_DE,'') and
  nvl(a.QOS_Certification_DE,'')=nvl(b.QOS_Certification_DE,'') and
  nvl(a.QOS_Device_Error_Code_DE,'')=nvl(b.QOS_Device_Error_Code_DE,'') and
  nvl(a.QOS_Device_Error_Message_DE,'')=nvl(b.QOS_Device_Error_Message_DE,'') and
  nvl(a.QOS_Geographic_Area_DE,'')=nvl(b.QOS_Geographic_Area_DE,'') and
  nvl(a.QOS_HGU_Model_DE,'')=nvl(b.QOS_HGU_Model_DE,'') and
  nvl(a.QOS_STB_Model_DE,'')=nvl(b.QOS_STB_Model_DE,'') and
  nvl(a.QOS_Multicast_Group_DE,'')=nvl(b.QOS_Multicast_Group_DE,'') and
  nvl(a.ORIGIN_FILE_NAME_DE,'')=nvl(b.ORIGIN_FILE_NAME_DE,'') and
  nvl(a.fecha_archivo,'')=nvl(b.fecha_archivo,'') and
  nvl(a.Latitude_de,'')=nvl(b.Latitude_de,'') and
  nvl(a.Longitude_de,'')=nvl(b.Longitude_de,'') and
  nvl(a.CDN_Request_Type_de,'')=nvl(b.CDN_Request_Type_de,'') and
  nvl(a.Media_Resource_de,'')=nvl(b.Media_Resource_de,'') and
  nvl(a.Playback_Status_de,'')=nvl(b.Playback_Status_de,'') and
  nvl(cast(a.day as string),'')= nvl(cast(b.day as string),'')
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.Playtime_CA is null and
  b.Traffic_CA is null and
  b.Seek_count_CA is null and
  b.Effective_Playtime_CA is null and
  b.Pause_Time_CA is null and
  b.Avg_Seek_Time_CA is null and
  b.Join_Time_CA is null and
  b.interruptions_CA is null and
  b.Buffer_Ratio_NR is null and
  b.Buffer_Time_CA is null and
  b.Avg_Bitrate_NR is null and
  b.Happiness_Score_NR is null and
  b.Avg_Buffer_Duration_NR is null and
  b.Playback_Stalls_NR is null and
  b.Unicast_Channel_Change_Mean_Time_NR is null and
  b.Multicast_Channel_Join_Mean_Time_NR is null and
  b.View_Lost_Packets_CA is null and
  b.View_Fixed_Packets_CA is null and
  b.View_Packet_Loss_CA is null and
  b.Lost_Bursts_CA is null and
  b.Fixed_Bursts_CA is null and
  b.Failed_Bursts_CA is null and
  b.View_Avg_RSSI_NR is null and
  b.View_Avg_Physical_Rate_NR is null and
  b.View_Avg_Channel_interference_NR is null and
  b.Startup_Error_Count_CA is null and
  b.In_Stream_Error_Total_CA is null and
  b.Media_Duration_NR is null and
  b.Completion_Rate_NR is null and
  b.End_Time_FH is null and
  b.Start_Time_FH is null and
  b.Play_Head_NR is null and
  b.Country_DE is null and
  b.City_DE is null and
  b.Postal_Code_DE is null and
  b.State_Province_DE is null and
  b.Region_DE is null and
  b.ISP_DE is null and
  b.Connection_Type_DE is null and
  b.CDN_DE is null and
  b.CDN_Node_Host_DE is null and
  b.ASN_NR is null and
  b.Viewing_Mode_DE is null and
  b.Title_DE is null and
  b.Streaming_protocol_DE is null and
  b.Resource_Domain_DE is null and
  b.Type_DE is null and
  b.Domain_DE is null and
  b.Device_DE is null and
  b.Device_Type_data_filter_DE is null and
  b.Device_Vendor_DE is null and
  b.Device_Model_DE is null and
  b.Browser_DE is null and
  b.Browser_Version_DE is null and
  b.OS_DE is null and
  b.OS_Version_DE is null and
  b.Plugin_Version_DE is null and
  b.Player_DE is null and
  b.Player_Version_DE is null and
  b.Token_DE is null and
  b.Metadata_DE is null and
  b.Error_Metadata_DE is null and
  b.Rendition_DE is null and
  b.Error_Name_DE is null and
  b.Error_Description_DE is null and
  b.Crash_Status_DE is null and
  b.User_ID_NR is null and
  b.Transaction_ID_DE is null and
  b.IP_DE is null and
  b.IP_Version_DE is null and
  b.NUMBER_INSTANCE_DE is null and
  b.SELECTED_QUALITY_DE is null and
  b.CODE_INSTANCE_DE is null and
  b.DEVICE_TYPE_DE is null and
  b.VERSION_DE is null and
  b.CONNECTION_DE is null and
  b.COMMERCIALIZATION_TYPE_DE is null and
  b.Dimension_8_DE is null and
  b.Dimension_9_DE is null and
  b.Dimension_10_DE is null and
  b.QOS_Network_DE is null and
  b.QOS_TV_Model_DE is null and
  b.QOS_Origin_DE is null and
  b.QOS_Device_DE is null and
  b.QOS_MAC_Address_DE is null and
  b.QOS_Topology_Level1_DE is null and
  b.QOS_Topology_Level2_DE is null and
  b.QOS_Topology_Level3_DE is null and
  b.QOS_Topology_Level4_DE is null and
  b.QOS_Topology_Level5_DE is null and
  b.QOS_Topology_Level1_Type_Name_DE is null and
  b.QOS_Topology_Level2_Type_Name_DE is null and
  b.QOS_Topology_Level3_Type_Name_DE is null and
  b.QOS_Topology_Level4_Type_Name_DE is null and
  b.QOS_Topology_Level5_Type_Name_DE is null and
  b.QOS_Topology_Level2_Slot_DE is null and
  b.QOS_Topology_Level2_Port_DE is null and
  b.QOS_Topology_Level3_Slot_DE is null and
  b.QOS_Topology_Level3_Port_DE is null and
  b.QOS_Topology_Level4_Slot_DE is null and
  b.QOS_Topology_Level4_Port_DE is null and
  b.QOS_Topology_Level5_Slot_DE is null and
  b.QOS_Topology_Level5_Port_DE is null and
  b.QOS_Boot_ROM_version_DE is null and
  b.QOS_HDMI_Format_DE is null and
  b.QOS_Local_PVR_DE is null and
  b.QOS_Software_version_DE is null and
  b.QOS_Unique_user_DE is null and
  b.QOS_Content_type_DE is null and
  b.QOS_Certification_DE is null and
  b.QOS_Device_Error_Code_DE is null and
  b.QOS_Device_Error_Message_DE is null and
  b.QOS_Geographic_Area_DE is null and
  b.QOS_HGU_Model_DE is null and
  b.QOS_STB_Model_DE is null and
  b.QOS_Multicast_Group_DE is null and
  b.ORIGIN_FILE_NAME_DE is null and
  b.fecha_archivo is null and
  b.Latitude_de is null and
  b.Longitude_de is null and
  b.CDN_Request_Type_de is null and
  b.Media_Resource_de is null and
  b.Playback_Status_de is null and 
  b.day is null ;
  