INSERT INTO TRAFICO.GVP_OTT_Devices PARTITION (day) 
SELECT 
  a.CREATIONDATE,
  a.DEVICETYPE,
  a.MIBID,
  a.NAME,
  a.DEVICEID,
  a.EXTERNALID_MAC,
  a.ENABLED,
  a.USERID,
  a.USERUNIQUEID,
  a.USEREMAIL,
  a.NOTIFICATIONTOKEN,
  a.TOKENENABLED,
  a.MASTER,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_OTT_Devices_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_OTT_Devices WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(a.DEVICETYPE,'') = nvl(b.DEVICETYPE,'') and
  nvl(cast(a.MIBID as string),'') = nvl(cast(b.MIBID as string),'') and
  nvl(a.NAME,'') = nvl(b.NAME,'') and
  nvl(a.DEVICEID,'') = nvl(b.DEVICEID,'') and
  nvl(a.EXTERNALID_MAC,'') = nvl(b.EXTERNALID_MAC,'') and
  nvl(cast(a.ENABLED as string),'') = nvl(cast(b.ENABLED as string),'') and
  nvl(cast(a.USERID as string),'') = nvl(cast(b.USERID as string),'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.USEREMAIL,'') = nvl(b.USEREMAIL,'') and
  nvl(a.NOTIFICATIONTOKEN,'') = nvl(b.NOTIFICATIONTOKEN,'') and
  nvl(CAST(a.TOKENENABLED AS STRING),'') = nvl(CAST(b.TOKENENABLED AS STRING),'') and
  nvl(CAST(a.MASTER AS STRING),'') = nvl(CAST(b.MASTER AS STRING),'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(CAST(a.CREATIONDATE_FH_AR AS STRING),'') = nvl(CAST(b.CREATIONDATE_FH_AR AS STRING),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'') 
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.CREATIONDATE is null and
  b.DEVICETYPE is null and
  b.MIBID is null and
  b.NAME is null and
  b.DEVICEID is null and
  b.EXTERNALID_MAC is null and
  b.ENABLED is null and
  b.USERID is null and
  b.USERUNIQUEID is null and
  b.USEREMAIL is null and
  b.NOTIFICATIONTOKEN is null and
  b.TOKENENABLED is null and
  b.MASTER is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null;
  