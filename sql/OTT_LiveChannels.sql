INSERT INTO TRAFICO.GVP_OTT_LiveChannels PARTITION (day) 
SELECT
  a.CREATIONDATE,          
  a.ID,
  a.NAME_DE,
  a.CALLLETTER,
  a.EPGLIVECHANNELREFERENCEID,
  a.ACTIVE,
  a.PPV,
  a.DVR,
  a.TIMESHIFT,
  a.NUMBER_NR,
  a.REQUIRESPIN,
  a.SOURCE,
  a.DEVICETYPE,
  a.ISVISIBLE,
  a.ISPLAYABLE,
  a.QUALITY,
  a.CONTENTSELECTIONID,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_OTT_LiveChannels_fp a 
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_OTT_LiveChannels WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.ID as string),'') = nvl(cast(b.ID as string),'') and
  nvl(a.NAME_DE,'') = nvl(b.NAME_DE,'') and
  nvl(a.CALLLETTER,'') = nvl(b.CALLLETTER,'') and
  nvl(cast(a.EPGLIVECHANNELREFERENCEID as string),'') = nvl(cast(b.EPGLIVECHANNELREFERENCEID as string),'') and
  nvl(cast(a.ACTIVE as string),'') = nvl(cast(b.ACTIVE as string),'') and
  nvl(cast(a.PPV as string),'') = nvl(cast(b.PPV as string),'') and
  nvl(cast(a.DVR as string),'') = nvl(cast(b.DVR as string),'') and
  nvl(cast(a.TIMESHIFT as string),'') = nvl(cast(b.TIMESHIFT as string),'') and
  nvl(cast(a.NUMBER_NR as string),'') = nvl(cast(b.NUMBER_NR as string),'') and
  nvl(cast(a.REQUIRESPIN as string),'') = nvl(cast(b.REQUIRESPIN as string),'') and
  nvl(cast(a.SOURCE as string),'') = nvl(cast(b.SOURCE as string),'') and
  nvl(cast(a.DEVICETYPE as string),'') = nvl(cast(b.DEVICETYPE as string),'') and
  nvl(cast(a.ISVISIBLE as string),'') = nvl(cast(b.ISVISIBLE as string),'') and
  nvl(cast(a.ISPLAYABLE as string),'') = nvl(cast(b.ISPLAYABLE as string),'') and
  nvl(cast(a.QUALITY as string),'') = nvl(cast(b.QUALITY as string),'') and
  nvl(cast(a.CONTENTSELECTIONID as string),'') = nvl(cast(b.CONTENTSELECTIONID as string),'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR AS string),'') = nvl(cast(b.CREATIONDATE_FH_AR AS string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'') 
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.ID is null and
  b.NAME_DE is null and
  b.CALLLETTER is null and
  b.EPGLIVECHANNELREFERENCEID is null and
  b.ACTIVE is null and
  b.PPV is null and
  b.DVR is null and
  b.TIMESHIFT is null and
  b.NUMBER_NR is null and
  b.REQUIRESPIN is null and
  b.SOURCE is null and
  b.DEVICETYPE is null and
  b.ISVISIBLE is null and
  b.ISPLAYABLE is null and
  b.QUALITY is null and
  b.CONTENTSELECTIONID is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null;
  