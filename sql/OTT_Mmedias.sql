INSERT INTO TRAFICO.GVP_OTT_Mmedias PARTITION (day) 
SELECT 
  a.MOVIEID ,
  a.ID,
  a.QUALITY,
  a.TYPE_DE,
  a.LANGUAGE_DE,
  a.LANGUAGELIST,
  a.CDNSTATUS,
  a.PLAYTYPE,
  a.ORIGIN_FILE_NAME,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_OTT_Mmedias_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_OTT_Mmedias WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.MOVIEID as string),'') = nvl(cast(b.MOVIEID as string),'') and
  nvl(cast(a.ID as string),'') = nvl(cast(b.ID as string),'') and
  nvl(a.QUALITY,'') = nvl(b.QUALITY,'') and
  nvl(a.TYPE_DE,'') = nvl(b.TYPE_DE,'') and
  nvl(a.LANGUAGE_DE,'') = nvl(b.LANGUAGE_DE,'') and
  nvl(a.LANGUAGELIST,'') = nvl(b.LANGUAGELIST,'') and
  nvl(a.CDNSTATUS,'') = nvl(b.CDNSTATUS,'') and
  nvl(a.PLAYTYPE,'') = nvl(b.PLAYTYPE,'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.MOVIEID is null and
  b.ID is null and
  b.QUALITY is null and
  b.TYPE_DE is null and
  b.LANGUAGE_DE is null and
  b.LANGUAGELIST is null and
  b.CDNSTATUS is null and
  b.PLAYTYPE is null and
  b.ORIGIN_FILE_NAME is null and
  b.fecha_archivo is null and
  b.day is null;
  