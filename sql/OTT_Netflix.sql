set tez.queue.name=${TEZ_QUEUE};
INSERT INTO trafico.gvp_ott_netflix PARTITION (day)
SELECT
a.userid
,a.unique_user_id
,a.device_id
,a.time_stamp
,a.netflix_type
,a.duration
,a.ORIGIN_FILE_NAME
,a.fecha_archivo
,a.fecha_ingesta
,a.day
FROM trafico.gvp_ott_netflix_fp a
LEFT JOIN 
(SELECT * FROM trafico.gvp_ott_netflix WHERE day=${FECHA_A_PROCESAR}) b
ON
  nvl(cast(a.userid as string),'') = nvl(cast(b.userid as string),'') and
  nvl(a.unique_user_id,'') = nvl(b.unique_user_id,'') and
  nvl(cast(a.device_id as string),'') = nvl(cast(b.device_id as string),'') and
  nvl(cast(a.time_stamp as string),'') = nvl(cast(b.time_stamp as string),'') and
  nvl(a.netflix_type,'') = nvl(b.netflix_type,'') and
  nvl(a.duration,'') = nvl(b.duration,'') and 
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.fecha_ingesta as string),'') = nvl(cast(b.fecha_ingesta as string),'') and
  nvl(cast(a.day as string),'')= nvl(cast(b.day as string),'')
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and
  b.userid is null and
  b.unique_user_id is null and 
  b.device_id is null and
  b.time_stamp is null and
  b.netflix_type is null and
  b.duration is null and
  b.ORIGIN_FILE_NAME is null and
  b.fecha_archivo is null and
  b.fecha_ingesta is null 


