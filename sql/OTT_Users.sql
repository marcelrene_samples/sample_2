INSERT INTO TRAFICO.GVP_OTT_Users PARTITION (day) 
SELECT 
  a.CREATIONDATE,
  a.MIBID,
  a.USERUNIQUEID,
  a.USEREMAIL,
  a.SERVICETYPE,
  a.STATUS,
  a.PVR,
  a.GEOGRAPHICAREA,
  a.BLACKOUTAREA,
  a.TIMEZONE,
  a.SDMEDIASTREAMS,
  a.HDMEDIASTREAMS,
  a.DVBTTUNERS,
  a.CURRENTCREDITLIMIT,
  a.TYPEID,
  a.CREDITCARDSTATUS,
  a.SUBSCRIPTIONS,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_OTT_Users_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_OTT_Users WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.MIBID as string),'') = nvl(cast(b.MIBID as string),'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.USEREMAIL,'') = nvl(b.USEREMAIL,'') and
  nvl(a.SERVICETYPE,'') = nvl(b.SERVICETYPE,'') and
  nvl(a.STATUS,'') = nvl(b.STATUS,'') and
  nvl(cast(a.PVR as string),'') = nvl(cast(b.PVR as string),'') and
  nvl(cast(a.GEOGRAPHICAREA as string),'') = nvl(cast(b.GEOGRAPHICAREA as string),'') and
  nvl(cast(a.BLACKOUTAREA as string),'') = nvl(cast(b.BLACKOUTAREA as string),'') and
  nvl(cast(a.TIMEZONE as string),'') = nvl(cast(b.TIMEZONE as string),'') and
  nvl(cast(a.SDMEDIASTREAMS as string),'') = nvl(cast(b.SDMEDIASTREAMS as string),'') and
  nvl(cast(a.HDMEDIASTREAMS as string),'') = nvl(cast(b.HDMEDIASTREAMS as string),'') and
  nvl(cast(a.DVBTTUNERS as string),'') = nvl(cast(b.DVBTTUNERS as string),'') and
  nvl(cast(a.CURRENTCREDITLIMIT as string),'') = nvl(cast(b.CURRENTCREDITLIMIT as string),'') and
  nvl(cast(a.TYPEID as string),'') = nvl(cast(b.TYPEID as string),'') and
  nvl(cast(a.CREDITCARDSTATUS as string),'') = nvl(cast(b.CREDITCARDSTATUS as string),'') and
  nvl(a.SUBSCRIPTIONS,'') = nvl(b.SUBSCRIPTIONS,'')and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'') 
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.MIBID is null and
  b.USERUNIQUEID is null and
  b.USEREMAIL is null and
  b.SERVICETYPE is null and
  b.STATUS is null and
  b.PVR is null and
  b.GEOGRAPHICAREA is null and
  b.BLACKOUTAREA is null and
  b.TIMEZONE is null and
  b.SDMEDIASTREAMS is null and
  b.HDMEDIASTREAMS is null and
  b.DVBTTUNERS is null and
  b.CURRENTCREDITLIMIT is null and
  b.TYPEID is null and
  b.CREDITCARDSTATUS is null and
  b.SUBSCRIPTIONS is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and
  b.day is null;
  
  