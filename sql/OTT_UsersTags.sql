INSERT INTO TRAFICO.GVP_OTT_UsersTags PARTITION (day) 
SELECT 
  a.CREATIONDATE,      
  a.ID,
  a.TYPEID,
  a.USERUNIQUEID,
  a.NAME_DE,
  a.VALUE_DE,
  a.PRODUCTTYPE,
  a.PRODUCTID,
  a.ORIGIN_FILE_NAME,
  a.CREATIONDATE_FH_AR,
  a.fecha_archivo,
  a.fecha_ingesta,
  a.day
FROM TRAFICO.GVP_OTT_UsersTags_fp a
LEFT JOIN 
(SELECT * FROM TRAFICO.GVP_OTT_UsersTags WHERE day=${FECHA_A_PROCESAR}) b
on
  nvl(cast(a.CREATIONDATE as string),'') = nvl(cast(b.CREATIONDATE as string),'') and
  nvl(cast(a.ID as string),'') = nvl(cast(b.ID as string),'') and
  nvl(cast(a.TYPEID as string),'') = nvl(cast(b.TYPEID as string),'') and
  nvl(a.USERUNIQUEID,'') = nvl(b.USERUNIQUEID,'') and
  nvl(a.NAME_DE,'') = nvl(b.NAME_DE,'') and
  nvl(a.VALUE_DE,'') = nvl(b.VALUE_DE,'') and
  nvl(a.PRODUCTTYPE,'') = nvl(b.PRODUCTTYPE,'') and
  nvl(cast(a.PRODUCTID as string),'') = nvl(cast(b.PRODUCTID as string),'') and
  nvl(a.ORIGIN_FILE_NAME,'') = nvl(b.ORIGIN_FILE_NAME,'') and
  nvl(cast(a.CREATIONDATE_FH_AR as string),'') = nvl(cast(b.CREATIONDATE_FH_AR as string),'') and
  nvl(a.fecha_archivo,'') = nvl(b.fecha_archivo,'') and 
  nvl(cast(a.day as string),'') = nvl(cast(b.day as string),'')  
WHERE 
  a.fecha_proceso=${CURR_DATE} and 
  a.day = ${FECHA_A_PROCESAR} and 
  b.CREATIONDATE is null and
  b.ID is null and
  b.TYPEID is null and
  b.USERUNIQUEID is null and
  b.NAME_DE is null and
  b.VALUE_DE is null and
  b.PRODUCTTYPE is null and
  b.PRODUCTID is null and
  b.ORIGIN_FILE_NAME is null and
  b.CREATIONDATE_FH_AR is null and
  b.fecha_archivo is null and 
  b.day is null;
  