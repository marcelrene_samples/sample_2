#!/bin/bash

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

HOME_APP="$(dirname ${BASH_SOURCE[0]})"
HIVE_CONNECTION_STRING="jdbc:hive2://aphdpmngtp10.tmoviles.com.ar:2181,aphdpmngtp11.tmoviles.com.ar:2181,aphdpmngtp09.tmoviles.com.ar:2181,aphdpmngtp07.tmoviles.com.ar:2181,aphdpmngtp08.tmoviles.com.ar:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2;principal=hive/_HOST@TASA.COM"
DB_ASEG_INGRESOS="aseguramiento_ingresos"
DB_TRAFICO="trafico"

for SQL_FILE in $(ls ${HOME_APP}/*.sql); do

    beeline -u ${HIVE_CONNECTION_STRING} --silent=true --hivevar DBNAME_TRAFICO=${DB_TRAFICO} --hivevar DBNAME_ASEG_INGRESOS=${DB_ASEG_INGRESOS} -f ${SQL_FILE}

    if [ ${PIPESTATUS[0]} -ne 0 ]; then
        echo "$(date '+%d/%m/%Y %H:%M:%S') - [ERR] - Se produjo un error al ejecutar el script ${SQL_FILE}." | tee -a ${HOME_APP}/info.log
        exit 1
    else
        echo "$(date '+%d/%m/%Y %H:%M:%S') - [INF] - El script ${SQL_FILE} finalizo correctamente" | tee -a ${HOME_APP}/info.log
    fi

done

exit 0

