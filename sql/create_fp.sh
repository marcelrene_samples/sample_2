#!/bin/bash
# Este proceso se encarga de llamadas a beeline
# para cargar las tablas temporales con valores finales.
export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

INTERFAZ=$1   

bin_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/ && pwd )"/
source "${bin_path}""../cfg/$INTERFAZ.cfg" #Archvio de configuración con los parámetros de conexión
sql_fp_path=${sql_local_path}create_fp/
source "${bin_path}""utils_v2.sh" "${INTERFAZ}.cfg" "${INTERFAZ}"

execute "beeline --silent==true -u \"$path_beeline\" --hivevar TEZ_QUEUE=$queue -f \"${sql_fp_path}Create_fp_${INTERFAZ}.sql\"" "Se crea una tabla general para ${INTERFAZ}" "No se pudo crear tabla para ${INTERFAZ}"
