-- Tabla con datos crudos desde GVP/Argentina/IPTV/Devices*/
drop table trafico.gvp_iptv_devices_fp;
create table trafico.gvp_iptv_devices_fp(
  creationdate int,
  devicetype string,
  mibid int,
  name string,
  deviceid string,
  externalid_mac string,
  enabled int,
  userid int,
  useruniqueid string,
  useremail string,
  notificationtoken string,
  tokenenabled boolean,
  master boolean,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta string,
  day int)
PARTITIONED BY (
 fecha_proceso int
)
STORED AS ORC;