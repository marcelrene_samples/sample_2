-- Tabla con datos crudos desde GVP/Argentina/IPTV/
drop table trafico.gvp_iptv_livechannels_fp;
create table trafico.gvp_iptv_livechannels_fp(
  creationdate int,
  id int,
  name_de string,
  callletter string,
  epglivechannelreferenceid int,
  active boolean,
  ppv boolean,
  dvr boolean,
  timeshift boolean,
  number_nr int,
  requirespin boolean,
  source int,
  devicetype int,
  isvisible boolean,
  isplayable boolean,
  quality int,
  contentselectionid int,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;
