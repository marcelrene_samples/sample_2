-- Tabla con datos crudos desde GVP/Argentina/IPTV/
drop table trafico.gvp_iptv_mmedias_fp;
create table trafico.gvp_iptv_mmedias_fp(
  movieid int,
  id int,
  quality string,
  type_de string,
  language_de string,
  languagelist string,
  cdnstatus string,
  playtype string,
  origin_file_name string,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;
