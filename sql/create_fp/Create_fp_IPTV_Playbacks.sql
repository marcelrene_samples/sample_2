-- Tabla con datos crudos desde GVP/Argentina/IPTV/
drop table trafico.gvp_iptv_playbacks_fp;
create table trafico.gvp_iptv_playbacks_fp(
  datetime_dt int,
  producttype string,
  productname string,
  productid int,
  producername string,
  distributorname string,
  useraccount string,
  useruniqueid string,
  servicetype string,
  commercializationtype string,
  origin_file_name string,
  datetime_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
 fecha_proceso int
)
STORED AS ORC;