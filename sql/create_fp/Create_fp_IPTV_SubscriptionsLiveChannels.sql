-- Tabla con datos crudos desde GVP/Argentina/IPTV/
drop table trafico.gvp_iptv_subscriptionslivechannels_fp;
create table trafico.gvp_iptv_subscriptionslivechannels_fp(
  creationdate int,
  subscriptionid int,
  subscriptionname string,
  livechannelid int,
  livechannelname string,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;