-- Tabla con datos crudos desde GVP/Argentina/IPTV/
drop table trafico.gvp_iptv_userspreferences_fp;
create table trafico.gvp_iptv_userspreferences_fp(
  creationdate int,
  id int,
  owner_de int,
  devicetypeid int,
  useruniqueid string,
  name_de string,
  preferencename string,
  value_de string,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;