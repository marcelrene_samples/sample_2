-- Tabla con datos crudos desde GVP/Argentina/IPTV/
drop table trafico.gvp_iptv_userstags_fp;
create table trafico.gvp_iptv_userstags_fp(
  creationdate int,
  id int,
  typeid int,
  useruniqueid string,
  name_de string,
  value_de string,
  producttype string,
  productid int,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;