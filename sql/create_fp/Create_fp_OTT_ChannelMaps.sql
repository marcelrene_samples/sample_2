-- Tabla con datos crudos desde GVP/Argentina/OTT/
drop table trafico.gvp_ott_channelmaps_fp;
create table trafico.gvp_ott_channelmaps_fp(
  creationdate int,
  id int,
  name_de string,
  title string,
  livechannelid int,
  quality int,
  regionid int,
  regionname string,
  regiongeographicareacode int,
  technologiesid int,
  technologiesname string,
  technologiestitle string,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;

