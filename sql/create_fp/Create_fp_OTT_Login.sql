-- Tabla con datos crudos desde GVP/Argentina/OTT/
drop table trafico.gvp_ott_login_fp;
create table trafico.gvp_ott_login_fp(
  datetime_dt int,
  devicetypeused int,
  deviceid int,
  statusmessage string,
  statuscode int,
  usertype int,
  uniqueusercode string,
  method int,
  sessionid string,
  userid string,
  origin_file_name string,
  datetime_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;
