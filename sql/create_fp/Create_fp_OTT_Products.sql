-- Tabla con datos crudos desde GVP/Argentina/OTT/
drop table trafico.gvp_ott_products_fp;
create table trafico.gvp_ott_products_fp(
  creationdate int,
  id int,
  name_de string,
  type_de string,
  commercializationstart int,
  commercializationend int,
  status_nr int,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  commercializationstart_fh_ar timestamp,
  commercializationend_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;