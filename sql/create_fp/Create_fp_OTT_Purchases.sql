-- Tabla con datos crudos desde GVP/Argentina/OTT/
drop table trafico.gvp_ott_purchases_fp;
create table trafico.gvp_ott_purchases_fp(
  datetime_dt int,
  producttype string,
  productid int,
  product int,
  productname string,
  producername string,
  distributorname string,
  commercializationtype int,
  contentcategoryid int,
  tvodtypeid int,
  quality string,
  price int,
  useremail string,
  useruniqueid string,
  firstname string,
  lastname string,
  servicetype string,
  devicetypename string,
  paymenttype string,
  webpaytransactionid string,
  idwebpay string,
  authwebpaycode string,
  purchasestatus int,
  transactionhash string,
  syncmessage string,
  deviceid string,
  billingid string,
  origin_file_name string,
  datetime_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;

