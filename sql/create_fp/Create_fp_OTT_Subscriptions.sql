-- Tabla con datos crudos desde GVP/Argentina/OTT/
drop table trafico.gvp_ott_subscriptions_fp;
create table trafico.gvp_ott_subscriptions_fp(
  creationdate int,
  userid string,
  useruniqueid string,
  useremail string,
  status_de string,
  subscriptionid int,
  subscriptionname string,
  paymenttype int,
  datestart int,
  dateend int,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  datestart_fh_ar timestamp,
  dateend_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;
