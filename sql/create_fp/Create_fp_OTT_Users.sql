-- Tabla con datos crudos desde GVP/Argentina/OTT
drop table  trafico.gvp_ott_users_fp;
create table trafico.gvp_ott_users_fp(
  creationdate int,
  mibid int,
  useruniqueid string,
  useremail string,
  servicetype string,
  status string,
  pvr boolean,
  geographicarea int,
  blackoutarea int,
  timezone int,
  sdmediastreams int,
  hdmediastreams int,
  dvbttuners int,
  currentcreditlimit float,
  typeid int,
  creditcardstatus int,
  subscriptions string,
  origin_file_name string,
  creationdate_fh_ar timestamp,
  fecha_archivo string,
  fecha_ingesta timestamp,
  day int)
PARTITIONED BY (
  fecha_proceso int
)
STORED AS ORC;
