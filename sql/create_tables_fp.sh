#!/bin/bash

export HADOOP_CLIENT_OPTS="-Djline.terminal=jline.UnsupportedTerminal"

HOME_APP="$(dirname ${BASH_SOURCE[0]})"
HIVE_CONNECTION_STRING="jdbc:hive2://aphdpmngtp10.tmoviles.com.ar:2181,aphdpmngtp11.tmoviles.com.ar:2181,aphdpmngtp09.tmoviles.com.ar:2181,aphdpmngtp07.tmoviles.com.ar:2181,aphdpmngtp08.tmoviles.com.ar:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2;principal=hive/_HOST@TASA.COM"
HIVE_CONNECTION_STRING_DESA='jdbc:hive2://aphdpmnd01.tmoviles.com.ar:2181,aphdpmnd02.tmoviles.com.ar:2181,aphdpdnd07.tmoviles.com.ar:2181/;serviceDiscoveryMode=zooKeeper;zooKeeperNamespace=hiveserver2'

./create_fp.sh OTT_Movies
./create_fp.sh OTT_Youbora
./create_fp.sh OTT_UsersTags
./create_fp.sh OTT_SubscriptionsLiveChannels
./create_fp.sh OTT_UsersPreferences
./create_fp.sh OTT_Purchases
./create_fp.sh OTT_Subscriptions
./create_fp.sh OTT_Playbacks
./create_fp.sh OTT_Products
./create_fp.sh OTT_Mmedias
./create_fp.sh OTT_Login
./create_fp.sh OTT_Devices
./create_fp.sh OTT_LiveChannels
./create_fp.sh OTT_ChannelMaps
./create_fp.sh OTT_Users
./create_fp.sh IPTV_UsersTags
./create_fp.sh IPTV_Users
./create_fp.sh IPTV_UsersPreferences
./create_fp.sh IPTV_SubscriptionsLiveChannels
./create_fp.sh IPTV_Subscriptions
./create_fp.sh IPTV_Purchases
./create_fp.sh IPTV_LiveChannels
./create_fp.sh IPTV_Products
./create_fp.sh IPTV_Playbacks
./create_fp.sh IPTV_Movies
./create_fp.sh IPTV_Mmedias
./create_fp.sh IPTV_Login
./create_fp.sh IPTV_Devices
./create_fp.sh IPTV_ChannelMaps


