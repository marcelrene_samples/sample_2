INSERT OVERWRITE TABLE TRAFICO.GVP_IPTV_Mmedias_fp PARTITION (fecha_proceso)
SELECT
MOVIEID ,
ID,
QUALITY,
TYPE_DE,
LANGUAGE_DE,
LANGUAGELIST,
CDNSTATUS,
PLAYTYPE,
split(INPUT__FILE__NAME,'/')[8] as ORIGIN_FILE_NAME,
split(split(INPUT__FILE__NAME,'/')[8],'_')[2] fecha_archivo,
cast(from_unixtime(unix_timestamp(current_timestamp(),'yyyy-MM-dd hh:mm:ss'),'yyyy-MM-dd hh:mm:ss') as timestamp) fecha_ingesta,
translate(split(split(INPUT__FILE__NAME,'/')[8],'_')[2],"-", "") as day,
${CURR_DATE} as fecha_proceso
FROM TRAFICO.Ext_GVP_IPTV_Mmedias;
