INSERT OVERWRITE TABLE TRAFICO.GVP_IPTV_UsersTags_fp PARTITION (fecha_proceso)
SELECT 
CREATIONDATE,      
ID,
TYPEID,
USERUNIQUEID,
NAME_DE,
VALUE_DE,
PRODUCTTYPE,
PRODUCTID,
split(INPUT__FILE__NAME,'/')[8] as ORIGIN_FILE_NAME,
to_utc_timestamp(date_format(from_unixtime(CAST (CREATIONDATE AS INT)), 'yyyy-MM-dd HH:mm:ss'),'America/Buenos Aires')  as CREATIONDATE_FH_AR,
split(split(INPUT__FILE__NAME,'/')[8],'_')[2] fecha_archivo,
cast(from_unixtime(unix_timestamp(current_timestamp(),'yyyy-MM-dd hh:mm:ss'),'yyyy-MM-dd hh:mm:ss') as timestamp) fecha_ingesta,
translate(split(split(INPUT__FILE__NAME,'/')[8],'_')[2],"-", "") as day,
${CURR_DATE} as fecha_proceso
FROM TRAFICO.Ext_GVP_IPTV_UsersTags;