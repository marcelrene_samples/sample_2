set tez.queue.name=${TEZ_QUEUE};
INSERT OVERWRITE TABLE trafico.gvp_ott_netflix_fp PARTITION(fecha_proceso)
SELECT 
CASE WHEN userid = "" THEN -3 ELSE userid END AS userid
,CASE WHEN unique_user_id = "" THEN "No informado" ELSE unique_user_id END AS unique_user_id
,CASE WHEN device_id = "" THEN -3 ELSE device_id END AS device_id
,cast(from_unixtime(unix_timestamp(substr(time_stamp,1,19),"MM/dd/yyyy hh:mm:ss"),"yyyy-MM-dd hh:mm:ss") as timestamp) AS time_stamp
,CASE WHEN netflix_type = "" THEN "No informado" ELSE netflix_type END AS netflix_type
,CASE WHEN  duration = "" THEN "No informado" ELSE  duration END AS duration
,split(INPUT__FILE__NAME,'/')[8] AS ORIGIN_FILE_NAME
,substr(split(INPUT__FILE__NAME,'/')[8],41,10) fecha_archivo
,cast(from_unixtime(unix_timestamp(current_timestamp(),'yyyy-MM-dd hh:mm:ss'),'yyyy-MM-dd hh:mm:ss') as timestamp) fecha_ingesta
,COALESCE(from_unixtime(unix_timestamp(time_stamp,'MM/dd/yyyy'), 'yyyyMMdd'), 19000101) AS day
,${CURR_DATE} as fecha_proceso
FROM trafico.ext_gvp_ott_netflix