INSERT OVERWRITE TABLE TRAFICO.GVP_OTT_Playbacks_fp PARTITION(fecha_proceso)
SELECT 
DATETIME_DT,
PRODUCTTYPE,
PRODUCTNAME,
PRODUCTID,
PRODUCERNAME,
DISTRIBUTORNAME,
USERACCOUNT,
USERUNIQUEID,
SERVICETYPE,
COMMERCIALIZATIONTYPE,
split(INPUT__FILE__NAME,'/')[8] as ORIGIN_FILE_NAME,
to_utc_timestamp(date_format(from_unixtime(CAST (DATETIME_DT AS INT)), 'yyyy-MM-dd HH:mm:ss'),'America/Buenos Aires')  as DATETIME_FH_AR,
split(split(INPUT__FILE__NAME,'/')[8],'_')[2] fecha_archivo,
cast(from_unixtime(unix_timestamp(current_timestamp(),'yyyy-MM-dd hh:mm:ss'),'yyyy-MM-dd hh:mm:ss') as timestamp) fecha_ingesta,
from_unixtime(cast(DATETIME_DT as int),'yyyyMMdd') as DAY,
${CURR_DATE} as fecha_proceso
FROM TRAFICO.Ext_GVP_OTT_Playbacks;
