INSERT OVERWRITE TABLE TRAFICO.GVP_OTT_Products_fp PARTITION(fecha_proceso)
SELECT 
CREATIONDATE,
ID,
NAME_DE,
TYPE_DE,
COMMERCIALIZATIONSTART,
COMMERCIALIZATIONEND,
STATUS_NR,
split(INPUT__FILE__NAME,'/')[8] as ORIGIN_FILE_NAME,
to_utc_timestamp(date_format(from_unixtime(CAST (CREATIONDATE AS INT)), 'yyyy-MM-dd HH:mm:ss'),'America/Buenos Aires')  as CREATIONDATE_FH_AR,
to_utc_timestamp(date_format(from_unixtime(CAST (COMMERCIALIZATIONSTART AS INT)), 'yyyy-MM-dd HH:mm:ss'),'America/Buenos Aires')  as COMMERCIALIZATIONSTART_FH_AR,
to_utc_timestamp(date_format(from_unixtime(CAST (COMMERCIALIZATIONEND AS INT)), 'yyyy-MM-dd HH:mm:ss'),'America/Buenos Aires')  as COMMERCIALIZATIONEND_FH_AR,
split(split(INPUT__FILE__NAME,'/')[8],'_')[2] fecha_archivo,
cast(from_unixtime(unix_timestamp(current_timestamp(),'yyyy-MM-dd hh:mm:ss'),'yyyy-MM-dd hh:mm:ss') as timestamp) fecha_ingesta,
translate(split(split(INPUT__FILE__NAME,'/')[8],'_')[2],"-", "") as day,
${CURR_DATE} as fecha_proceso
FROM TRAFICO.Ext_GVP_OTT_Products;

