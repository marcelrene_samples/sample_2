-- Adición de  5 campos en tabla externa TRAFICO.Ext_GVP_KPI_Youbora y tabla final  trafico.GVP_kpi_Youbora*/
set tez.queue.name=${TEZ_QUEUE};
ALTER TABLE TRAFICO.Ext_GVP_KPI_Youbora ADD COLUMNS(Latitude_de string);
ALTER TABLE TRAFICO.Ext_GVP_KPI_Youbora ADD COLUMNS(Longitude_de string);
ALTER TABLE TRAFICO.Ext_GVP_KPI_Youbora ADD COLUMNS(CDN_Request_Type_de string);
ALTER TABLE TRAFICO.Ext_GVP_KPI_Youbora ADD COLUMNS(Media_Resource_de string);
ALTER TABLE TRAFICO.Ext_GVP_KPI_Youbora ADD COLUMNS(Playback_Status_de string);

ALTER TABLE trafico.GVP_kpi_Youbora ADD COLUMNS(Latitude_de string);
ALTER TABLE trafico.GVP_kpi_Youbora ADD COLUMNS(Longitude_de string);
ALTER TABLE trafico.GVP_kpi_Youbora ADD COLUMNS(CDN_Request_Type_de string);
ALTER TABLE trafico.GVP_kpi_Youbora ADD COLUMNS(Media_Resource_de string);
ALTER TABLE trafico.GVP_kpi_Youbora ADD COLUMNS(Playback_Status_de string);